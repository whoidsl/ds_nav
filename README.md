# README #

This repository contains the navigation software for the Sentry ROS upgrade

### Repository contents ###

* ds_nav: base class for all navigation nodes
* ds_deadreck: navigation node that inherits from ds_nav, and that implements the dvlnav-navest dead reckoning solution. Offsets and transformations are obtained through tf2, math is done with Eigen3.
* ds_fake_drive: python sources and scripts to generate sensor data on ROS topics consistent with a predefined trajectory. Publishes a sim pose to compare on rviz with the pose produced by the realtime navigation nodes
* navest_replay: generates sensor data on ROS topics based on actual real time data acquired during a previous Sentry dive
* ds_navaggregator: contains a dynamically reconfigurable navigation aggregation node

### Required additional repositories

sudo apt install ros-kinetic-hector-trajectory-server
sudo apt install libgeographic-dev
