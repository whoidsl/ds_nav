ds_navest: a node that converts ROS-world nav data to navest-compliant outputs:
- VPR (vehicle position report)
- VFR (vehicle fix report)
- DVZ (control loop input)
