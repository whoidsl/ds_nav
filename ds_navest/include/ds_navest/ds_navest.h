/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_NAVEST_H
#define DS_NAVEST_H

#include "ros/ros.h"
#include "ds_nav/ds_nav.h"
#include "sensor_msgs/NavSatFix.h"
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/Dvl.h>
#include <std_srvs/SetBool.h>

namespace ds_navest
{
class DsNavest final : public ds_nav::DsNav
{
public:
  DsNavest(int argc, char* argv[], const std::string& name);
  ~DsNavest() override;

  void onPhinsbinMsg(const ds_sensor_msgs::PhinsStdbin3::ConstPtr& msg);
  void onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg);

  void parseReceivedInput(const ds_core_msgs::RawData& bytes);

  bool service_req(const std_srvs::SetBool::Request& req,
		   std_srvs::SetBool::Response& resp);

protected:
  void setupSubscriptions() override;
  void setupParameters() override;
  void setupPublishers() override;
  void setupConnections() override;
  void setupServices() override;

private:
  ros::Subscriber phins_stdbin3_sub_;
  ros::Subscriber dvl_sub_;

  ds_sensor_msgs::Dvl m_dvl;

  double m_origin_lat;
  double m_origin_lon;
  double m_utm_zone;

  bool veh_phins_vpr_enabled;

  int ghost_phins_id;
  bool ghost_phins_enabled;

  bool enable_dvz_send_;

  bool use_additional_vfr;
  
  boost::shared_ptr<ds_asio::DsConnection> vpr;
  boost::shared_ptr<ds_asio::DsConnection> vfr;
  boost::shared_ptr<ds_asio::DsConnection> dvz;
  boost::shared_ptr<ds_asio::DsConnection> inssol;
  boost::shared_ptr<ds_asio::DsConnection> ins;
  boost::shared_ptr<ds_asio::DsConnection> insdata;
  boost::shared_ptr<ds_asio::DsConnection> vfr2;
  
  ros::ServiceServer cmd_serv_;

};
}

#endif
