/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "ds_navest/ds_navest.h"
#include <limits>

namespace ds_navest
{
  DsNavest::DsNavest(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
  {
    enable_dvz_send_ = false;
  }

  DsNavest::~DsNavest() = default;



  void DsNavest::setupSubscriptions(void)
  {
    DsNav::setupSubscriptions();

    auto phinsbin_topic = ros::param::param<std::string>("~phinsbin_topic", "/jason/sensors/phinsins/phinsbin");
    phins_stdbin3_sub_ = nodeHandle().subscribe<ds_sensor_msgs::PhinsStdbin3>(phinsbin_topic, 1, boost::bind(&DsNavest::onPhinsbinMsg, this, _1));

    auto dvl_topic = ros::param::param<std::string>("~dvl_topic", "/jason/sensors/dvl");
    dvl_sub_ = nodeHandle().subscribe<ds_sensor_msgs::Dvl>(dvl_topic, 1, boost::bind(&DsNavest::onDvlMsg, this, _1));
  }

  void DsNavest::setupParameters(void)
  {
    DsNav::setupParameters();

    m_origin_lat = ros::param::param<double>("~origin_lat", 0);
    m_origin_lon = ros::param::param<double>("~origin_lon", 0);
    m_utm_zone = ros::param::param<double>("~utm_zone", 0);

    veh_phins_vpr_enabled = ros::param::param<bool>("~veh_phins_vpr_enabled", false);
    ghost_phins_enabled = ros::param::param<bool>("~ghost_phins_enabled", true);
    ghost_phins_id = ros::param::param<int>("~ghost_phins_id", 5);

    
    setOrigin(m_origin_lat, m_origin_lon);
  }

  void DsNavest::setupPublishers(void)
  {
    DsNav::setupPublishers();

    //cmd_serv_ = nodeHandle().advertiseService<std_srvs::SetBool::Request, std_srvs::SetBool::Response>(
    //     ros::this_node::getName() + "/dvz_srv", boost::bind(&DsNavest::service_req, this, _1, _2));

  }

  void DsNavest::setupServices(void)
  {
    DsNav::setupServices();

    cmd_serv_ = nodeHandle().advertiseService<std_srvs::SetBool::Request, std_srvs::SetBool::Response>(
          ros::this_node::getName() + "/dvz_srv", boost::bind(&DsNavest::service_req, this, _1, _2));

  }

  void DsNavest::setupConnections(void)
  {
    DsNav::setupConnections();

    vpr = addConnection("vpr", boost::bind(&DsNavest::parseReceivedInput, this, _1));

    vfr = addConnection("vfr", boost::bind(&DsNavest::parseReceivedInput, this, _1));

    // If true, create a second vfr connection called vfr2 for sending to another additional network
    use_additional_vfr = ros::param::param<bool>("~use_additional_vfr", false);
    if (use_additional_vfr == true)
      {
	vfr2 = addConnection("vfr2", boost::bind(&DsNavest::parseReceivedInput, this, _1));
      }
    
    dvz = addConnection("dvz", boost::bind(&DsNavest::parseReceivedInput, this, _1));

    inssol = addConnection("inssol", boost::bind(&DsNavest::parseReceivedInput, this, _1));

    ins = addConnection("ins", boost::bind(&DsNavest::parseReceivedInput, this, _1));

    insdata = addConnection("insdata", boost::bind(&DsNavest::parseReceivedInput, this, _1));

  }

  void DsNavest::onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg)
  {
    m_dvl = *msg;
  }

  bool DsNavest::service_req(const std_srvs::SetBool::Request& req,
			     std_srvs::SetBool::Response& resp)
  {
    enable_dvz_send_ = req.data;
    ROS_ERROR_STREAM("TOGGLING DVZ: " << enable_dvz_send_);

    resp.success = 1;
    return true;
  }

  void DsNavest::onPhinsbinMsg(const ds_sensor_msgs::PhinsStdbin3::ConstPtr& msg)
  {
    // Make VPR and VFR
    //ROS_ERROR_STREAM("ds_navest: received phinsbin");

    auto time = ros::Time::now().toBoost();
    auto facet = new boost::posix_time::time_facet("%Y/%m/%d %H:%M:%S");
    
    auto vfr_os = std::ostringstream{};
    vfr_os.imbue(std::locale(vfr_os.getloc(), facet));
    vfr_os << std::fixed << std::setprecision(7);

    auto vpr_os = std::ostringstream{};
    vpr_os.imbue(std::locale(vpr_os.getloc(), facet));
    vpr_os << std::fixed << std::setprecision(7);

    int mode = 9;
    int veh_ghost_id = ghost_phins_id;
    int veh_id = 0; //Assumes vehicle ID is 0
    double alt = 5.4;
    int depthOrAlt = 0;
    double cog = 0;
    double sog = 0;
    //ROS_ERROR_STREAM("Lon: " << msg->longitude);
    double lat = msg->latitude;
    double lon = ((msg->longitude > 180) ? (msg->longitude - 360.0) : (msg->longitude));
    //ROS_ERROR_STREAM("Lon: " << msg->longitude);

    vfr_os << "VFR " << time << " " << mode << " " << veh_id << " SOLN_PHINS " << lon << " " << msg->latitude << " " << -msg->altitude << " " << alt << " " << depthOrAlt << " " << sog << " " << cog << "\r\n";
    vfr->send(vfr_os.str());
    if (use_additional_vfr == true)
      {
	vfr2->send(vfr_os.str());
      }
    
   if (veh_phins_vpr_enabled) { 
    vpr_os << "VPR " << time << " " << "Phins " << veh_id << " " << lon << " " << msg->latitude << " " << msg->altitude << " " << msg->heading << " " << msg->pitch << " " << msg->roll << "\r\n";
    vpr->send(vpr_os.str());
   }

    if (ghost_phins_enabled) {
    auto vfr_ghost_os = std::ostringstream{};
    vfr_ghost_os.imbue(std::locale(vfr_ghost_os.getloc(), facet));
    vfr_ghost_os << std::fixed << std::setprecision(7);
    vfr_ghost_os << "VFR " << time << " " << mode << " " << veh_ghost_id << " SOLN_PHINS " << lon << " " << msg->latitude << " " << -msg->altitude << " " << alt << " " << depthOrAlt << " " << sog << " " << cog << "\r\n";
    vfr->send(vfr_ghost_os.str());

    auto vpr_ghost_os = std::ostringstream{};
    vpr_ghost_os.imbue(std::locale(vpr_ghost_os.getloc(), facet));
    vpr_ghost_os << std::fixed << std::setprecision(7);
    vpr_ghost_os << "VPR " << time << " " << "Phins " << veh_ghost_id << " " << lon << " " << msg->latitude << " " << msg->altitude << " " << msg->heading << " " << msg->pitch << " " << msg->roll << "\r\n";
    vpr->send(vpr_ghost_os.str());
    }

    double x;
    double y;
    alt = (m_dvl.altitude > 0) ? m_dvl.altitude : 0;
    //alt = 10.0;
    std::tie(x, y) = latlon2xy(msg->latitude, msg->longitude);
    auto dvz_os = std::ostringstream{};
    dvz_os.imbue(std::locale(dvz_os.getloc(), facet));
    dvz_os << std::fixed << std::setprecision(7);
    dvz_os << "DVZ " << time << " DOP LXY ROV " << x << " " << y << " " << msg->altitude << " "  << msg->heading << " " << msg->pitch << " " << msg->roll << " ";
    dvz_os << -msg->velocity_NEU[1] << " " << msg->velocity_NEU[0] << " " << -msg->velocity_NEU[2] << " ";
    dvz_os <<  msg->heading_rate << " " << msg->pitch_rate << " " << msg->roll_rate << " ";
    dvz_os << -msg->body_accel_XVn[1] << " " << msg->body_accel_XVn[0] << " " << -msg->body_accel_XVn[2] << " ";
    dvz_os <<  m_utm_zone << " " << m_origin_lat << " " << m_origin_lon << " ";
    dvz_os <<  (m_dvl.num_good_beams >=3) << " " << (m_dvl.num_good_beams >=3) << " ";
    dvz_os <<  (int)m_dvl.num_good_beams << " " << (int)m_dvl.num_good_beams << " ";
    dvz_os <<  alt << " " << m_dvl.range[0] << " " << m_dvl.range[1] << " ";
    dvz_os <<  m_dvl.range[2] << " " << m_dvl.range[3] << "\r\n";
    dvz_os.str();
    //ROS_ERROR_STREAM("ds_navest dvz: " << dvz_os.str());
    if (enable_dvz_send_)
      {
	dvz->send(dvz_os.str());
      }

    // auto inssol_os = std::ostringstream{};
    // inssol_os << std::fixed << std::setprecision(7);
    // inssol_os << "INSSOL " << lat << " " << lon << "\n";
    // ROS_ERROR_STREAM("inssol: " << inssol_os.str());
    // inssol->send(inssol_os.str());

    auto inssol_os = std::ostringstream{};
    inssol_os << std::fixed << std::setprecision(7);
    inssol_os << "INSSOL " << lat << " " << lon << " " << msg->altitude << " ";
    inssol_os << msg->heading << " " << msg->pitch << " " << msg->roll << " ";
    inssol_os << msg->velocity_NEU[1] << " " << msg->velocity_NEU[0] << " " << msg->velocity_NEU[2] << " ";

    inssol_os <<  0 << " " << 0 << " " << 0 << " ";
    inssol_os <<  0 << " " << 0 << " " << 0 << " ";
    
    //inssol_os <<  msg->heading_rate << " " << msg->pitch_rate << " " << msg->roll_rate << " ";
    //inssol_os << msg->body_accel_XVn[0] << " " << -msg->body_accel_XVn[1] << " " << -msg->body_accel_XVn[2] << " ";
    //ROS_ERROR_STREAM("inssol: " << inssol_os.str());
    inssol->send(inssol_os.str());

    // Simple ins positioning
    auto ins_os = std::ostringstream{};
    ins_os << std::fixed << std::setprecision(7);
    ins_os << "INS " << lon << " " << lat << " " << msg->altitude << "\n";
    ins->send(ins_os.str());

    // Ins output for data systems including positioning and attitude
    auto insdata_os = std::ostringstream{};
    insdata_os << std::fixed << std::setprecision(7);
    insdata_os << "INSDATA " << lon << " " << lat << " " << msg->altitude << " ";
    insdata_os << msg->heading << " " << msg->pitch << " " << msg->roll << "\n";
    insdata->send(insdata_os.str());
    
  }

  void DsNavest::parseReceivedInput(const ds_core_msgs::RawData& bytes)
  {
    ; // Do nothing - this node is supposed to be output only
  }

}
