/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_GPSNAV_H
#define DS_GPSNAV_H

#include "ds_nav/ds_nav.h"
#include "ds_nav_msgs/NavState.h"
#include "ds_nmea_msgs/Gga.h"
#include "ds_nmea_msgs/Vtg.h"
#include "ds_sensor_msgs/Gyro.h"
#include "ds_util/angle.h"

#include <sensor_msgs/NavSatFix.h>

namespace ds_gpsnav
{
class DsGpsnav : public ds_nav::DsNav
{
public:
  DsGpsnav();

  DsGpsnav(int argc, char* argv[], const std::string& name);
  ~DsGpsnav() override;

  void initialize(void);

  void onGgaMsg(const ds_nmea_msgs::Gga::ConstPtr& msg);
  void onVtgMsg(const ds_nmea_msgs::Vtg::ConstPtr& msg);
  void onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg);

  ds_nav_msgs::NavState getState(void);

protected:
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;

private:
  ros::Subscriber gga_sub_;
  ros::Subscriber vtg_sub_;
  ros::Subscriber gyro_sub_;

  ros::Publisher state_pub_;
  ros::Publisher navsat_pub_;
  
  ds_nmea_msgs::Gga gga_;
  ds_nmea_msgs::Vtg vtg_;
  ds_sensor_msgs::Gyro gyro_;

  ds_nav_msgs::NavState state_;
};
}

#endif
