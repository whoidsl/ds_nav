/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_gpsnav/ds_gpsnav.h"

namespace ds_gpsnav
{
DsGpsnav::~DsGpsnav() = default;

DsGpsnav::DsGpsnav() : DsNav()
{
}

DsGpsnav::DsGpsnav(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
{
}

void DsGpsnav::initialize(void)
{
  state_.lat = 0.0;
  state_.lon = 0.0;
  state_.easting = 0.0;
  state_.northing = 0.0;
  state_.down = 0.0;
  state_.surge_u = 0.0;
  state_.sway_v = 0.0;
  state_.heave_w = 0.0;
}

void DsGpsnav::onGgaMsg(const ds_nmea_msgs::Gga::ConstPtr& msg)
{
  gga_ = *msg;

  state_.lat = msg->latitude;
  state_.lon = msg->longitude;
  std::tie(state_.easting, state_.northing) = latlon2xy(state_.lat, state_.lon);
  state_.down = -msg->antenna_alt;

  state_.header.stamp = ros::Time::now();

  state_pub_.publish(state_);

  sensor_msgs::NavSatFix navs;
  // This uses the time in the nmea string
  navs.header.stamp = msg->timestamp;
  // This uses the current time
  //navs.header.stamp = ros::Time::now();
  if (msg->latitude_dir == 'N')
    {
      navs.latitude = msg->latitude;
    }
  else
    {
      navs.latitude = -msg->latitude;
    }
  if (msg->longitude_dir == 'E')
    {
      navs.longitude = msg->longitude;
    }
  else
    {
      navs.longitude = -msg->longitude;
    }
  navs.altitude = msg->antenna_alt;
  navs.status.status = msg->fix_quality - 1;
  navs.status.service = navs.status.SERVICE_GPS;
  navs.position_covariance[0] = (msg->hdop * 5) * (msg->hdop * 5);
  navs.position_covariance[4] = (msg->hdop * 5) * (msg->hdop * 5);
  navs.position_covariance[8] = 100;
  // Only hdop is used
  navs.position_covariance_type = navs.COVARIANCE_TYPE_APPROXIMATED;

  navsat_pub_.publish(navs);
  
}

void DsGpsnav::onVtgMsg(const ds_nmea_msgs::Vtg::ConstPtr& msg)
{
  vtg_ = *msg;

  // Need to transform the track made good and ground speed to vehicle
  // frame to determine surge and sway velocities. To do that we need gyro data
  double angular_separation =
      ds_util::angular_separation_radians(msg->track_degrees_true * M_PI / 180.0, gyro_.heading);
  double knots_to_ms = 0.514444;
  state_.surge_u = msg->speed_knots * knots_to_ms * cos(angular_separation);
  state_.sway_v = msg->speed_knots * knots_to_ms * sin(angular_separation);
}

void DsGpsnav::onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg)
{
  gyro_ = *msg;
}

ds_nav_msgs::NavState DsGpsnav::getState(void)
{
  return state_;
}

void DsGpsnav::setupParameters()
{
  DsNav::setupParameters();

  double origin_lat = ros::param::param<double>(ros::this_node::getNamespace() + "/origin_lat", 0);
  double origin_lon = ros::param::param<double>(ros::this_node::getNamespace() + "/origin_lon", 0);
  ROS_INFO_STREAM("Origin lat: " << origin_lat << " Origin lon: " << origin_lon);
  setOrigin(origin_lat, origin_lon);
}

void DsGpsnav::setupSubscriptions()
{
  DsNav::setupSubscriptions();

  std::string gga_topic = ros::param::param<std::string>("~gga_topic", "0");
  gga_sub_ = nodeHandle().subscribe<ds_nmea_msgs::Gga>(gga_topic, 1, boost::bind(&DsGpsnav::onGgaMsg, this, _1));
  std::string vtg_topic = ros::param::param<std::string>("~vtg_topic", "0");
  vtg_sub_ = nodeHandle().subscribe<ds_nmea_msgs::Vtg>(vtg_topic, 1, boost::bind(&DsGpsnav::onVtgMsg, this, _1));
  std::string gyro_topic = ros::param::param<std::string>("~gyro_topic", "0");
  vtg_sub_ = nodeHandle().subscribe<ds_sensor_msgs::Gyro>(gyro_topic, 1, boost::bind(&DsGpsnav::onGyroMsg, this, _1));
}

void DsGpsnav::setupPublishers()
{
  DsNav::setupPublishers();

  std::string state_topic = ros::param::param<std::string>("~state_topic", "/gpsnav_state");
  state_pub_ = nodeHandle().advertise<ds_nav_msgs::NavState>(state_topic, 1);

  navsat_pub_ = nodeHandle().advertise<sensor_msgs::NavSatFix>(ros::this_node::getName() + "/navsat", 10, false);
}
}
