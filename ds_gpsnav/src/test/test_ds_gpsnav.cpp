/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_gpsnav/ds_gpsnav.h"
#include <gtest/gtest.h>

// This allows us to call protected functions
class DsGpsnavFixture : public ds_gpsnav::DsGpsnav, public testing::Test
{
protected:
  void SetUp() override
  {
    setup();
  }
};

TEST_F(DsGpsnavFixture, checkState)
{
  boost::shared_ptr<ds_nmea_msgs::Gga> gga(new ds_nmea_msgs::Gga);
  boost::shared_ptr<ds_nmea_msgs::Vtg> vtg(new ds_nmea_msgs::Vtg);
  boost::shared_ptr<ds_sensor_msgs::Gyro> gyro(new ds_sensor_msgs::Gyro);
  ds_nav_msgs::NavState state;

  gga->latitude = 48.01798636593;
  gga->longitude = 2.01340493099;
  gga->antenna_alt = 5.0;

  vtg->track_degrees_true = 45.0;
  vtg->speed_knots = 2.0;

  gyro->heading = 80.0 * M_PI / 180.0;

  onGyroMsg(gyro);
  onVtgMsg(vtg);
  onGgaMsg(gga);
  state = getState();

  ASSERT_FLOAT_EQ(state.lat, 48.01798636593);
  ASSERT_FLOAT_EQ(state.lon, 2.01340493099);
  ASSERT_FLOAT_EQ(state.easting, 1000.0);
  ASSERT_FLOAT_EQ(state.northing, 2000.0);
  ASSERT_FLOAT_EQ(state.down, -5.0);

  ASSERT_FLOAT_EQ(state.surge_u, 1.028888 * cos(35 * M_PI / 180.0));
  ASSERT_FLOAT_EQ(state.sway_v, 1.028888 * sin(35 * M_PI / 180.0));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "ds_gpsnav");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
};
