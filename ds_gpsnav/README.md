This node generates a navstate message from gps data to send to the nav aggregator. Also computes vehicle-frame surge and sway (for real time vehicle control) from world-frame cog and sog that gps provides

Input subscriptions (examples in this README assume namespace is "/sentry/nav", and node name "gpsnav"):
- /sentry/nav/gpsnav/gga_topic: input topic from parsed gga string from gps driver node
- /sentry/nav/gpsnav/vtg_topic: input topic from parsed vtg string from gps driver node
- /sentry/nav/gpsnav/gyro_topic: gyro topic from gyro driver node

Input parameters (examples in this README assume namespace is "/sentry/nav", and node name "gpsnav"):
- /sentry/nav/origin_lat: origin lat. Note that this is in the nav namespace, not in the node namespace, so if this is defined already in the nav namespace it does not need to be defined again for this node
- /sentry/nav/origin_lon: origin lon. Note that this is in the nav namespace, not in the node namespace, so if this is defined already in the nav namespace it does not need to be defined again for this node

Output publishers (examples in this README assume namespace is "/sentry/nav", and node name "gpsnav"):
- /sentry/nav/gpsnav/state_topic: navstate topic that is sent to the nav aggregator

