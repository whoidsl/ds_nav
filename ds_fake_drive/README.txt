README for ds_fake_drive.py ROS node
Author: Theo Guerin (tguerin@whoi.edu), 15-16 Jan 2018

-------------------------------------------------------------------------------
OVERVIEW:

The ds_fake_drive.py node is a Python module that outputs mock vehicle data
given certain inputs that the user issues in commands in a Python script. For
example, given vehicle velocity and heading, the module outputs vehicle
position over the course of a given duration of time. For a numerical example,
given a velocity of 1 m/s along the vehicle's x-axis, with a heading of 0
degrees and time of 1 second, the module outputs vehicle position that is
translated 1 meter along the x-axis of the global frame.

This node publishes ROS messages to topics based on the computed mock data.
The messages are: DepthPressure, Ins, Dvl, Clock, and PoseStamped. Clock
messages are published if the user desires to publish simulation time to the
ROS clock so that other nodes can run based on this time. By publishing
PoseStamped messages, this node enables the user to visualize the fake
vehicle's pose and motion in rviz.

-------------------------------------------------------------------------------
USAGE:

The user uses this module by writing Python scripts that contain the driving
instructions for a vehicle. Some example scripts are offered in the scripts/
directory.

After importing the ds_fake_drive module, the user can create an instance of
the Vehicle class. This object has a number of methods for the user to use:

    1. drive(self, t, u=0, v=0, w=0, hdg=0, pitch=0, roll=0):
        - As the name implies, this method makes the vehicle move according to
          a duration (t), velocities in the vehicle frame's x, y, and z
          directions (u, v, and w, respectively), and a heading (hdg) in
          degrees. Currently not supported are pitch and roll rotation inputs.
          Note that the arguments above listed with their default values (which
          are all 0) are optional arguments that can be omitted in the user's
          call to this method.

    2. set_time_increment(increment):
        - Set the desired time increment between data outputs. increment should
          be an int or a float.

    3. set_sim_speed_factor(factor):
        - Set the desired speed factor at which to output the simulated data.
          For example, a value of 2 will output the data at twice the speed of
          real time.

    4. state():
        - Print the vehicle's current world pose to the terminal.

    5. set_pose(x, y, z, hdg=None, pitch=None, roll=None):
        - Set the vehicle's world pose.

    6. set_clock_publishing(choice):
        - Publish elapsed time during a drive call to the /clock topic if this
          method is called with choice=True.

-------------------------------------------------------------------------------
RVIZ VISUALIZATION:

To visualize the fake vehicle's motion, the user can use rviz with the
following steps:

    1. In a terminal, run the command: rosrun rviz rviz
    2. In rviz, click the button labeled "Add"
    3. In the dialog box that appears, select "Pose" and click "OK"
    4. Under "Displays", find the newly added "Pose" display type
    5. Under "Pose", find the "Topic" field and select /ds_fake_drive/sim_poses
       from the dropdown menu

-------------------------------------------------------------------------------
POTENTIAL BUGS:

 >> It is possible that the vehicle and/or global frames are off. There could
    be an issue of sign/direction.

-------------------------------------------------------------------------------
DEVELOPMENT NOTES:

 >> Currently not supported are pitch and roll rotation inputs to the drive
    method.
 >> This module should probably be placed in a Python package with __init__.py
    and setup.py files. This would likely allow for improved importing of the
    module, rather than what is currently written in example scripts:

        # "Hacked" import for testing:
        import sys
        sys.path.append('../src')
        from ds_fake_drive import Vehicle

