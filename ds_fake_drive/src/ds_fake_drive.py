#!/usr/bin/env python
# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Originally started being written 11 Jan 2018 by Theo Guerin
#                                                 theocguerin@gmail.com

import rospy
import math
import numpy as np
import time
from decimal import Decimal as D
from ds_sensor_msgs.msg import DepthPressure, Ins, Dvl, SoundSpeed
from rosgraph_msgs.msg import Clock
from geometry_msgs.msg import PoseStamped
import tf

# TODO at some point: implement pitch and roll rotations

class Vehicle(object):
    def __init__(self):
        rospy.init_node('ds_fake_drive', anonymous=False)
        self.publish_clock_bool = False
        self.sim_speed_factor = 1.0 # default to real time
        self.set_time_increment(0.05) # seconds
        # Set initial position:
        self.set_pose(0., 0., 0., hdg=0., pitch=0., roll=0.)
        # Consider working on formatting, as data could exceed character limit.
        # Truncate? Round? Represent in scientific notation? Extend padding of
        # all other fields to match? Or, just keep reasonable widths, as is:
        output_format_l1 = '---------------------------------------------\n'
        output_format_l2 = 'Time (s):  {time:>8}\n'
        output_format_l3 = 'Position in world coordinate system (meters):\n'
        output_format_l4 = '    x: {x:>12.2f}\n'
        output_format_l5 = '    y: {y:>12.2f}\n'
        # depth (z) format implictly limited to tens of thousands
        # (8 characters = 5 non-decimal places + . + 2 decimal places).
        # Could be thousands:
        output_format_l6 = '    z:     {z:>8.2f}\n'
        output_format_l7 = 'Orientation (degrees):\n'
        output_format_l8 = '    Heading: {hdg:>6.2f}\n'
        output_format_l9 = '    Pitch:   {pitch:>6.2f}\n'
        output_format_l10 = '    Roll:    {roll:>6.2f}\n'
        self.output_format = (output_format_l1 + output_format_l2 +
                              output_format_l3 + output_format_l4 +
                              output_format_l5 + output_format_l6 +
                              output_format_l7 + output_format_l8 +
                              output_format_l9 + output_format_l10)
        self.output_format_no_time = (output_format_l1 + output_format_l3 +
                                      output_format_l4 + output_format_l5 +
                                      output_format_l6 + output_format_l7 +
                                      output_format_l8 + output_format_l9 +
                                      output_format_l10)
        self.init_ros_publishers()
        self.state(0.)
        self.wall_time = D('0')

    def drive(self, t, u=0, v=0, w=0, hdg=0, pitch=0, roll=0, good_beams=4):
        '''
        - Drive vehicle with 4 degrees of freedom:
            1-3.    Translation in x, y, and z vehicle coordinates
            4.      Rotation about the vehicle's z axis (determined by heading)
        - Eventually, implement pitch and roll
        - t is the duration, in seconds, for which to drive the Vehicle with
          the given inputs
        - u, v, and w are velocity (m/s) in x, y, and z directions,
          respectively, in vehicle coordinates
        - hdg is given in degrees by the user, to be converted to radians
          for calculations
        '''
        self.good_beams = good_beams
        self.heading_deg = float(hdg)
        self.pitch_deg = float(pitch)
        self.roll_deg = float(roll)
        heading_rad = math.radians(hdg)
        # Vehicle velocity vector in vehicle coordinates:
        # Column vector:
        self.vel_vehicle = np.matrix([[float(u)],
                                      [float(v)],
                                      [float(w)]])
        # Use Decimal object to avoid small float addition errors:
        elapsed_time = D('0')
        tw = time.time()
        # Rotate velocity vector from vehicle coordinates into world
        # coordinates and then use this rotated vector to get position in
        # the while loop, rather than rotating each position vector
        self.vel_world = self.rotate_matrix(self.vel_vehicle, heading_rad)
        while (elapsed_time < t) and (not rospy.is_shutdown()):
            # Integration of velocity to get position, with constant velocity
            # (i.e., v = v_avg):
            # s = s_0 + v*t  ->  s - s_0 = delta_pos_vehicle = v * delta_t
            delta_pos_world = self.vel_world*self.delta_t
            self.pos_world += delta_pos_world
            cur_wall_time = time.time()
            # Sleeping system adapted from navest_replay.py
            sleep_time = tw + self.delta_t_delay - cur_wall_time
            if sleep_time >= 0:
                time.sleep(sleep_time)
                #rospy.sleep(sleep_time)
            tw += self.delta_t_delay
            elapsed_time += self.delta_t_dec
            self.wall_time += self.delta_t_dec
            self.publish_ros_msgs(elapsed_time)
            if self.publish_clock_bool:
                # from_sec() seems capable of handling Decimal object as arg:
                self.pub_clock.publish(rospy.Time.from_sec(elapsed_time))
            self.state(elapsed_time)
            # TODO: Consider implementing total elapsed time since first
            # drive call

    def set_time_increment(self, increment):
        # Set delta t for data frequency
        self.delta_t = increment
        # Use Decimal object to avoid small float addition errors
        # in self.drive:
        self.delta_t_dec = D(str(self.delta_t))
        self._set_delta_t_delay()

    def set_sim_speed_factor(self, factor):
        self.sim_speed_factor = factor
        self._set_delta_t_delay()

    def _set_delta_t_delay(self):
        self.delta_t_delay = self.delta_t / self.sim_speed_factor

    def rotate_matrix(self, mat, hdg, pitch=None, roll=None):
        # TODO: Make this work for all rotations (i.e., account for pitch
        # and roll. Get rid of '=None' for pitch and roll arguments)
        return self.rotate_z(mat, hdg)

    def rotate_z(self, mat, hdg):
        # Rotate about the z-axis (a heading rotation)
        # hdg should be in radians
        rotation_z = np.matrix([[math.cos(hdg), -math.sin(hdg), 0],
                                [math.sin(hdg), math.cos(hdg), 0],
                                [0, 0, 1]])
        return rotation_z*mat

    def state(self, elapsed_time=None):
        x, y, z = unpack_npmatrix_3x1(self.pos_world)
        if elapsed_time is not None:
            print self.output_format.format(x=x, y=y, z=z,
                                        hdg=self.heading_deg,
                                        pitch=self.pitch_deg,
                                        roll=self.roll_deg,
                                        time=elapsed_time)
        else:
            print self.output_format_no_time.format(x=x, y=y, z=z,
                                        hdg=self.heading_deg,
                                        pitch=self.pitch_deg,
                                        roll=self.roll_deg)

    def set_pose(self, x, y, z, hdg=None, pitch=None, roll=None):
        '''
        Set vehicle's world pose.
        This value is initialized in self.__init__()
        '''
        self.pos_world = np.matrix([[float(x)],
                                    [float(y)],
                                    [float(z)]])
        if hdg is not None:
            self.heading_deg = float(hdg)
        if pitch is not None:
            self.pitch_deg = float(pitch)
        if roll is not None:
            self.roll_deg = float(roll)

    def init_ros_publishers(self):
        self.pub_dp = rospy.Publisher('/sentry/sensors/paro/depth',
                                            DepthPressure, queue_size=1)
        self.pub_ins = rospy.Publisher('/sentry/sensors/phins/ins',
                                            Ins, queue_size=1)
        self.pub_dvl = rospy.Publisher('/sentry/sensors/rdi300/dvl',
                                            Dvl, queue_size=1)
        self.pub_ss = rospy.Publisher('/sentry/sensors/resonsvp70/sound_speed',
                                            SoundSpeed, queue_size=1)
        self.pub_pose_stamped = rospy.Publisher('/ds_fake_drive/sim_poses',
                                            PoseStamped, queue_size=1)
        #self.broadcaster = tf.TransformBroadcaster()

    def publish_ros_msgs(self, t):
        paro_msg = DepthPressure()
        phins_msg = Ins()
        dvl_msg = Dvl()
        ss_msg = SoundSpeed()
        # Position components in world frame:
        x, y, z = unpack_npmatrix_3x1(self.pos_world)
        # Velocity components in world frame:
        u, v, w = unpack_npmatrix_3x1(self.vel_world)
        # Velocity components in vehicle frame:
        u_v, v_v, w_v = unpack_npmatrix_3x1(self.vel_vehicle)
        secs, nsecs = secs_and_nsecs_dec(t)

        # SS msg
        ss_msg.speed = 1500
        ss_msg.ds_header.io_time.secs = secs
        ss_msg.ds_header.io_time.nsecs = nsecs
        ss_msg.header.stamp.secs = secs
        ss_msg.header.stamp.nsecs = nsecs
        
        # Fill out fields of paro_msg:
        paro_msg.depth = z
        paro_msg.ds_header.io_time.secs = secs
        paro_msg.ds_header.io_time.nsecs = nsecs
        paro_msg.header.stamp.secs = secs
        paro_msg.header.stamp.nsecs = nsecs

        # Fill out fields of phins_msg:
        phins_msg.roll = self.roll_deg
        phins_msg.pitch = self.pitch_deg
        phins_msg.heading = self.heading_deg
        # Velocity components in global frame, where x, y, and z correspond
        # to E-N-U (East, North, and Up), respectively
        # (as described in Ins.msg file):
        # TODO: Verify that linear_velocity.x corresponds to East, .y to North,
        # and .z to Up
        phins_msg.linear_velocity.x = v     # v is East
        phins_msg.linear_velocity.y = u     # u is North
        phins_msg.linear_velocity.z = -w    # -w is Up, as w is Down
        phins_msg.ds_header.io_time.secs = secs
        phins_msg.ds_header.io_time.nsecs = nsecs
        phins_msg.header.stamp.secs = secs
        phins_msg.header.stamp.nsecs = nsecs

        # Fill out fields of dvl_msg:
        # TODO: Is the velocity field in this message in world frame or vehicle
        # frame? Can it be set by coordinate_mode? What about velocity_mode?
        # Also, velocity or raw_velocity?
        dvl_msg.coordinate_mode = dvl_msg.DVL_COORD_INSTRUMENT
        # Rotate velocity by 45 degrees based on DVL sensor positioning:
        # SS - changed to -135 degrees to test new dvl rotation value in DsDeadreck
        print u_v
        print v_v
        print w_v
        rotated_velocity = self.rotate_z(np.matrix([[u_v],
                                                    [v_v],
                                                    [w_v]]), math.radians(-45))
        v_rotated, u_rotated, w_rotated = unpack_npmatrix_3x1(rotated_velocity)
        #u_rotated = -0.7
        #v_rotated = -0.7
        #w_rotated = 0
        print u_rotated
        print v_rotated
        print w_rotated
        dvl_msg.velocity.x = u_rotated
        dvl_msg.velocity.y = v_rotated
        dvl_msg.velocity.z = -w_rotated
        dvl_msg.num_good_beams = self.good_beams
        # Sound speed m/s, set as constant for simplicity for now at least:
        dvl_msg.speed_sound = 1500.0
        dvl_msg.ds_header.io_time.secs = secs
        dvl_msg.ds_header.io_time.nsecs = nsecs
        dvl_msg.header.stamp.secs = secs
        dvl_msg.header.stamp.nsecs = nsecs
        print self.wall_time
        print float(self.wall_time)
        dvl_msg.dvl_time = float(self.wall_time)

        pose_stamped_msg = self.prepare_pose_stamped_msg(secs, nsecs, x, y, z)

#        self.broadcaster.sendTransform(
#                        (x, y, z),
#                        tf.transformations.quaternion_from_euler(
#                                math.radians(self.roll_deg),
#                                math.radians(self.pitch_deg),
#                                math.radians(self.heading_deg)),
#                        rospy.Time.from_sec(t),
#                        'base_link',
#                        'world')

        self.pub_dp.publish(paro_msg)
        self.pub_ins.publish(phins_msg)
        self.pub_dvl.publish(dvl_msg)
        self.pub_ss.publish(ss_msg)
        self.pub_pose_stamped.publish(pose_stamped_msg)

    def set_clock_publishing(self, choice):
        self.publish_clock_bool = choice
        if self.publish_clock_bool:
            # Initialize clock publisher. Note that the /use_sim_time ROS
            # parameter needs to be set to true in order to be able to run
            # other ROS nodes with the ROS simulation clock:
            self.pub_clock = rospy.Publisher('/clock', Clock, queue_size=1)
            # A new instance of this publisher is created each time
            # set_clock_publishing is called with choice=True. Could change it
            # so that only one instance is created the first time that this
            # method is called with choice=True.

    def prepare_pose_stamped_msg(self, secs, nsecs, x, y, z):
        # For rviz
        msg = PoseStamped()
        quaternion = tf.transformations.quaternion_from_euler(
                            math.radians(self.roll_deg),
                            math.radians(self.pitch_deg),
                            math.radians(self.heading_deg))
        msg.pose.position.x = x
        msg.pose.position.y = y
        msg.pose.position.z = z
        msg.pose.orientation.x = quaternion[0]
        msg.pose.orientation.y = quaternion[1]
        msg.pose.orientation.z = quaternion[2]
        msg.pose.orientation.w = quaternion[3]
        # Note that the timestamp seconds are not seconds since epoch, unlike
        # what the header message type calls for by convention
        msg.header.stamp.secs = secs
        msg.header.stamp.nsecs = nsecs
        msg.header.frame_id = 'odom_dr'
        return msg


def unpack_npmatrix_3x1(mat):
    mat_list = mat.tolist()
    a1 = mat_list[0][0]
    a2 = mat_list[1][0]
    a3 = mat_list[2][0]
    return (a1, a2, a3)

def secs_and_nsecs_dec(t):
    # t should be a Decimal object
    # t rounded to the nanosecond:
    t = round(t, 9)
    return (int(t), int(1e9 * (t - int(t))))

