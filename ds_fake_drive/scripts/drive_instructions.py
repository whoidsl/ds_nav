#!/usr/bin/env python
# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Originally started being written 11 Jan 2018 by Theo Guerin
#                                                 theocguerin@gmail.com

#import rospy

# "Hacked" import for testing:
import sys
sys.path.append('../src')
from ds_fake_drive import Vehicle

sentry = Vehicle()

sentry.set_time_increment(0.1)
#sentry.set_sim_speed_factor(2)
#sentry.set_position(1.0, 1.0, 0.0)

sentry.set_clock_publishing(True)

#sentry.set_pose(1,2,3,4,5,6)
#sentry.state()

# Drive in a 1-meter by 1-meter square:
for i in range(200):
    sentry.drive(t=1, u=1.0, v=0.0, hdg=90)
    sentry.drive(t=1, u=1.0, v=0.0, hdg=180)
    sentry.drive(t=1, u=0.0, v=1.0, hdg=180)
    sentry.drive(t=1, u=-1.0, v=0.0, hdg=180)

#sentry.set_time_increment(1.0)
#sentry.set_clock_publishing(True)
#sentry.drive(t=5.0, u=1.0, v=1.0, w=1.0, hdg=30)
#sentry.set_clock_publishing(False)
#sentry.drive(t=10.0, u=1.0, v=1.0, w=1.0, hdg=30)
#sentry.set_clock_publishing(True)
#sentry.drive(t=10.0, u=1.0, v=1.0, w=1.0, hdg=30)


#sentry.set_position(1,1,1, hdg=58)
#sentry.drive(t=180, u=1.0)


#sentry.set_sim_speed_factor(1)
#import time
#start = time.time()
#sentry.drive(t=20, u=1)
#end = time.time()
#print end-start # should be just a little bit greater than 20 seconds



