/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_DP_H
#define DS_DP_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "ds_nav/ds_nav.h"
#include "ds_nav/ds_nav_sensor.h"
#include "ds_base/ds_process.h"
#include "std_srvs/SetBool.h"

#include "ds_nmea_parsers/Vtg.h"
#include "ds_nmea_parsers/Gga.h"
#include "ds_nmea_parsers/Hdt.h"
#include "boost/date_time/posix_time/posix_time.hpp"
#include <boost/date_time/gregorian/gregorian.hpp>


#define RADIANS		(57.295779513082320876798154814105)		/* degrees/radian */
#define RTOD         RADIANS
#define DTOR         (1.0/RTOD)
#define METERS_NMILE	1852.0	/* meters/nautical mile */
#define SECS_HOUR 3600
#define MSEC_TO_KNOTS(a)	((a/METERS_NMILE)*SECS_HOUR)
#define KNOTS_TO_MSEC(a)	((a*METERS_NMILE)/SECS_HOUR)
#define MIN_DP_TYPE        NO_DP
#define	MAX_DP_TYPE        FAUX_GPS
#define TRAJECTORY_TIME_CONSTANT   90.0
#define METERS_FOOT 	0.3048	/* meters/foot */
#define MAX_REF_SPEED 1.3

namespace ds_dp
{
  typedef struct {
    float  latitude; // units are radians
    float  longitude;
    int	    num_satellites;
    double  hdop;
    double  altitude;
    double  geoid_separation;
    double  age;
    int	    valid;
    double  utc_time;
    int	    quality ;
    double  geoidal_separation;
    int     utcHours;
    int     utcMinutes;
    int     utcSeconds;
  } gpgga_t;

enum eDP_TYPE{
 NO_DP,
 FAUX_GPS
};

class DsDp : public ds_nav::DsNav
{
public:
  DsDp();

  DsDp(int argc, char* argv[], const std::string& name);
  ~DsDp() override;

protected:
  void initialize();
  
  void setupParameters() override;
  void setupConnections() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupTimers() override;
  void setupServices() override;
  void setup() override;

  void on_in_msg(const ds_core_msgs::RawData& bytes);

  void on_bridge_in_msg(const ds_core_msgs::RawData& bytes);

  void on_ui_in_msg(const ds_core_msgs::RawData& bytes);

  void on_gyro_in_msg(const ds_core_msgs::RawData& bytes);

  void on_vfr_in_msg(const ds_core_msgs::RawData& msg);

  void make_ui_update_msg(void);

  bool init_cmd(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& res);

  void onDpTimer(const ros::TimerEvent& event);


  void setGoal(double x, double y);
  void setGoalToRangeBearing(double inrange, double inbearing);
  void setRefToShip();
  void setOrigintoReference();
  void setGoalToRef();
  void setGoalToLatLon(double lat, double lon);
  void setDPType(eDP_TYPE theDpType);
  void incrementGoal(double xIncrement, double yIncrement);
  void computeReference();
  void setSpeedWidget(double newSpeed);
  void setActualReferenceSpeed(double theSpeed);
  void updateRefToGoal();
  void setTimeToGoal(double theTime);

  // Legacy NMEA
  unsigned short compute_nmea_checksum(char *msg);

  gpgga_t parseGpggaFromMsg(ds_nmea_msgs::Gga &msg);
  gpgga_t parse_gpgga(char *input_string);

  int fmt_gpgga(gpgga_t *g, char *string, char *inTimeString);

private:
  // ********** CONNECTIONS
  // INPUT: GPS strings - OUTPUT: modified GPS strings for the DP system
  boost::shared_ptr<ds_asio::DsConnection> m_conn;

  // Shadow connection for debugging
  boost::shared_ptr<ds_asio::DsConnection> m_debug_conn;

  // BRIDGE RASPI connection
  boost::shared_ptr<ds_asio::DsConnection> m_bridge_conn;

  // Navigator RASPI connection
  boost::shared_ptr<ds_asio::DsConnection> m_navigator_conn;

  // INPUT: commands from UI - OUTPUT: status updates to UI
  boost::shared_ptr<ds_asio::DsConnection> m_ui_conn;

  // INPUT: HEADING strings - OUTPUT passes heading to UI and bridge
  boost::shared_ptr<ds_asio::DsConnection> m_gyro_conn;

  // INPUT: VFR strings - OUTPUT passes navest corrected gga to DP system
  boost::shared_ptr<ds_asio::DsConnection> m_vfr_conn;

  // ********** PARAMETERS
  //list of strings to passthrough
  std::vector<std::string> passthrough_strings;

  ros::NodeHandle nh;

  // param that decides if to actually send out strings to dp system
  bool dp_enabled;

  // param that flags true when dp is initialized
  bool dp_init;

  // param that decides if we're using raw or navest corrected GPS stringsi (raw=0/navest=1)
  bool dp_gps_nav_corrected_mode;

  std::stringstream nav_corrected_ss;
  
  // just store the last parsed gga nmea msg
  std::string last_parsed_gga;

  std::string nav_corrected_gps;

  ds_nmea_msgs::Gga gga_msg;


  // ********** SERVICES
  // Initialize DP (set local origin)
  ros::ServiceServer m_init_srv;

  // Enable DP service
  ros::ServiceServer m_enable_srv;

  // Set goal lat lon service
  ros::ServiceServer m_goal_lat_lon_srv;

  // Set goal range bearing service
  ros::ServiceServer m_goal_range_bearing_srv;
  
  // ********** TIMERS
  // DP out timer loop
  ros::Timer m_dp_tmr;

  // Internal state
  bool m_enabled;
  double m_ex;
  double m_ey;


  // Algorithm state
  double referenceX;
  double referenceY;
  double antiReferenceX;
  double antiReferenceY;
  double goalX;
  double goalY;
  double shipVelX;
  double shipVelY;
  double shipSpeed;
  double shipCog;
  double lastShipLocalX;
  double lastShipLocalY;
  eDP_TYPE dpType;
  unsigned long updateCount;
  double currentTimeToGoal;
  std::string actualTimeToGoal;
  std::string actualDistToGoal;
  double actualTimeGoalMin;
  double actualTimeGoalSec;
  double actualDistGoalMeters;
  double shipHeading;
  double shipLocalX;
  double shipLocalY;
  double dpOriginX;
  double dpOriginY;
  double shipAntennaX;
  double shipAntennaY;
  double referenceSpeed;
  double reasonableReferenceSpeed;
  double shipGyroHdg;
  gpgga_t lastGpgga;
  double dpUpdateInterval;
  ros::Time lastGPS;
  ros::Time currentGPSTime;
  std::string lastNavestLat;
  std::string lastNavestLon;
  
};
}

#endif
