/**
 * Copyright 2018 Woods Hole Oceanographic Institution
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <ds_nmea_parsers/util.h>
#include "ds_util/int_to_hex.h"

#include "ds_dp/ds_dp.h"

namespace ds_dp
{
DsDp::~DsDp() = default;

DsDp::DsDp() : DsNav()
{
  initialize();
}

DsDp::DsDp(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
{
  initialize();
}

void DsDp::initialize()
{
  // Start disabled
  m_enabled = 0;

  // Internal state variables
  referenceX = 0.0;
  referenceY = 0.0;
  goalX = 0.0;
  goalY = 0.0;
  shipVelX = 0.0;
  shipVelY = 0.0;
  shipSpeed = 0.0;
  shipCog = 0.0;
  lastShipLocalX = 0.0;
  lastShipLocalY = 0.0;
  shipLocalX = 0.0;
  shipLocalY = 0.0;
  dpType = NO_DP;
  updateCount = 0;
}

void DsDp::setupConnections()
{
  DsNav::setupConnections();

  m_conn = addConnection("dp_connection", boost::bind(&DsDp::on_in_msg, this, _1));

  m_bridge_conn = addConnection("dp_bridge_connection", boost::bind(&DsDp::on_bridge_in_msg, this, _1));

  m_navigator_conn = addConnection("dp_navigator_connection", boost::bind(&DsDp::on_bridge_in_msg, this, _1));

  m_ui_conn = addConnection("dp_ui_connection", boost::bind(&DsDp::on_ui_in_msg, this, _1));

  m_gyro_conn = addConnection("dp_gyro_connection", boost::bind(&DsDp::on_gyro_in_msg, this, _1));

  m_vfr_conn = addConnection("dp_vfr_connection", boost::bind(&DsDp::on_vfr_in_msg, this, _1));

  m_debug_conn = addConnection("dp_debug_connection", boost::bind(&DsDp::on_in_msg, this, _1));
}

void DsDp::setupParameters()
{
  DsNav::setupParameters();

  if (!nodeHandle().getParam(ros::this_node::getName() + "/passthrough_strings", passthrough_strings))
  {
    ROS_FATAL_STREAM("MUST specify passthrough_strings vector parameter!");
    ROS_BREAK();
  }

  if (!nodeHandle().getParam(ros::this_node::getName() + "/dp_enabled", dp_enabled))
  {
    ROS_FATAL_STREAM("MUST specify dp_enabled bool parameter!");
    ROS_BREAK();
  }
  if (!nodeHandle().getParam(ros::this_node::getName() + "/dp_init", dp_init))
  {
    ROS_WARN_STREAM("NO DP INIT PARAM");
  }
  if (!nodeHandle().getParam(ros::this_node::getName() + "/dp_gps_nav_corrected_mode", dp_gps_nav_corrected_mode))
  {
    ROS_ERROR_STREAM("NO DP GPS MODE PARAM");
  }
}

void DsDp::setupSubscriptions()
{
  DsNav::setupSubscriptions();
}

void DsDp::setupPublishers()
{
  DsNav::setupPublishers();
}

void DsDp::setupTimers()
{
  DsNav::setupTimers();

  // This should be 1.0 or the same frequency of incoming gps fixes
  if (!nodeHandle().getParam(ros::this_node::getName() + "/" + "dp_update_interval", dpUpdateInterval))
  {
    ROS_FATAL_STREAM("MUST specify dp_update_interval double parameter!");
    ROS_BREAK();
  }
  dpUpdateInterval = 1.0;

  m_dp_tmr = nodeHandle().createTimer(ros::Duration(1.0), boost::bind(&DsDp::onDpTimer, this, _1));
}

void DsDp::setupServices()
{
  DsNav::setupServices();

  m_init_srv = nodeHandle().advertiseService<std_srvs::SetBoolRequest, std_srvs::SetBoolResponse>(
      "init", boost::bind(&DsDp::init_cmd, this, _1, _2));
}

void DsDp::setup()
{
  DsNav::setup();
}

// SERVICES
bool DsDp::init_cmd(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& res)
{
  bool init_req = req.data;
  if (init_req)
  {
    dp_init = 1;
    auto lat = lastGpgga.latitude;
    auto lon = lastGpgga.longitude;

    // Reset the origin at the last gpgga location
    ROS_ERROR_STREAM("ds_dp: Setting origin to lat: " << lat << " " << lon);
    setOrigin(lat, lon);

    // Recompute the ship position with the new origin (new ship position should be 0)
    ROS_ERROR_STREAM("lastGpgga lat: " << lastGpgga.latitude << " and lastGpgga lon: " << lastGpgga.longitude);
    std::tie(shipLocalX, shipLocalY) = latlon2xy(lastGpgga.latitude, lastGpgga.longitude);
    ROS_ERROR_STREAM("shipLocalX: " << shipLocalX << " and shipLocalY: " << shipLocalY);
    // Update internal algorithm values
    setRefToShip();
    setOrigintoReference();
    setGoalToRef();
    res.success = true;
    res.message = "init";
  }
  else
  {
    dp_init = 0;
    res.success = true;
    res.message = "deinit";
  }
  return true;
}

// TIMERS
void DsDp::onDpTimer(const ros::TimerEvent& event)
{
  gpgga_t outgoingGPGGA;
  char stringToSend[256];

  // QDateTime	timeNow = QDateTime::currentDateTime().toUTC();
  auto timeNow = ros::Time::now();
  auto timeNowBoost = timeNow.toBoost();
  auto facet = new boost::posix_time::time_facet("yyyy/MM/dd hh:mm:ss.zzz");
  auto os = std::ostringstream{};
  os << timeNowBoost;
  os.imbue(std::locale(os.getloc(), facet));
  std::string timeString = os.str();
  // QString timeString = timeNow.toUTC().toString("yyyy/MM/dd hh:mm:ss.zzz");
  if (FAUX_GPS == dpType)
  {
    int hrs = timeNowBoost.time_of_day().hours();
    int min = timeNowBoost.time_of_day().minutes();
    int secs = timeNowBoost.time_of_day().seconds();
    outgoingGPGGA.utcHours = hrs;
    outgoingGPGGA.utcMinutes = min;
    outgoingGPGGA.utcSeconds = secs;

    computeReference();
    outgoingGPGGA.quality = 2;

    // dpOriginX and dpOriginY are XY points of ship at origin
    //  shipAntennaX and shipAntennaY are current ship XY points
    double perturbedX = dpOriginX + shipAntennaX - referenceX;

    double perturbedY = dpOriginY + shipAntennaY - referenceY;

    antiReferenceX = perturbedX;
    antiReferenceY = perturbedY;

    std::tie(outgoingGPGGA.latitude, outgoingGPGGA.longitude) = xy2latlon(antiReferenceX, antiReferenceY);

    outgoingGPGGA.num_satellites = lastGpgga.num_satellites;
    outgoingGPGGA.hdop = lastGpgga.hdop;
    outgoingGPGGA.altitude = lastGpgga.altitude;
    outgoingGPGGA.geoidal_separation = lastGpgga.geoidal_separation;
  }
  else
  {
    memcpy(&outgoingGPGGA, &lastGpgga, sizeof(gpgga_t));
  }
  if (referenceSpeed < 0.005)
  {
    reasonableReferenceSpeed = 0.0;
  }
  else
  {
    reasonableReferenceSpeed = referenceSpeed;
  }

  if (dp_enabled && !dp_init)
  {
    ROS_WARN_STREAM_ONCE("DGPS enabled, but not init...passing on gpgga strings!");
    m_conn->send(last_parsed_gga);
    m_debug_conn->send(last_parsed_gga);
  }
  if (dp_enabled && dp_init)
  {
    ROS_WARN_STREAM_ONCE("DGPS ENABLED AND INITIALIZED");
    int len = fmt_gpgga(&outgoingGPGGA, stringToSend, (char*)timeString.c_str());
    std::string outCmd(stringToSend);
    // ROS_WARN_STREAM("gpgga out: "<<stringToSend);
    m_conn->send(outCmd);
    m_debug_conn->send(outCmd);
  }

  updateRefToGoal();

  double dx = referenceX - shipLocalX;
  double dy = referenceY - shipLocalY;
  double rx = dx;
  double ry = dy;
  double distance = sqrt(dx * dx + dy * dy);
  double bearing = RTOD * atan2(dx, dy);
  if (bearing < 0.0)
  {
    bearing += 360.;
  }
  // exWyLabel->setText("ex: "  + QString::number(dx,'f',1) + " wy: " + QString::number(dy,'f',1));
  // Store ex and ey for state update later on
  m_ex = dx;
  m_ey = dy;

  // rangeBearingErrorLabel->setText("range: " + QString::number(distance,'f',1) + " bearing: " +
  // QString::number(bearing,'f',1));
  dx = goalX - shipLocalX;
  dy = goalY - shipLocalY;
  distance = sqrt(dx * dx + dy * dy);
  actualDistGoalMeters = distance;
  bearing = RTOD * atan2(dx, dy);

  if (bearing < 0.0)
  {
    bearing += 360.0;
  }

  auto jdpRecord = std::ostringstream{};
  jdpRecord << std::fixed << std::setprecision(0) << "JDP " << timeString << " " << dpType << " "
            << std::to_string(referenceSpeed) << " " << bearing << " " << std::to_string(shipSpeed) << " "
            << shipGyroHdg << " " << dx << " " << dy << " " << shipHeading << " " << currentTimeToGoal << "\n";

  m_bridge_conn->send(jdpRecord.str());
  m_navigator_conn->send(jdpRecord.str());
  make_ui_update_msg();
}

// DATA INPUTS
void DsDp::on_in_msg(const ds_core_msgs::RawData& bytes)
{
  auto stream = std::stringstream(std::string{ std::begin(bytes.data), std::end(bytes.data) } + nav_corrected_gps);
  auto nmea_msg = std::string{};

  while (std::getline(stream, nmea_msg))
  {
    auto begin_it = nmea_msg.find('$');
    if (begin_it == std::string::npos)
    {
      continue;
    }

    if (begin_it != 0)
    {
      nmea_msg = nmea_msg.substr(begin_it);
    }
    uint8_t calc_checksum = ds_nmea_msgs::calculate_checksum(nmea_msg);
    // Loop over messages types of interest.
    if (nmea_msg.find("GGA") != std::string::npos)
    {
      // Only run VFR check if nav correction mode active
      if (dp_gps_nav_corrected_mode)
      {
        // Got a GGA, is it from VFR or RAW?
        if (nmea_msg.find("VFR") != std::string::npos)
        {
          if (!ds_nmea_msgs::from_string(gga_msg, nmea_msg))
          {
            ROS_WARN_STREAM("Unable to parse $--GGA from string: " << nmea_msg);
            continue;
          }
        }
      }
      else
      {
        ROS_WARN_STREAM("Looking for raw GGA");
        if (nmea_msg.find("VFR") != std::string::npos)
        {
          continue;
        }
        if (!ds_nmea_msgs::from_string(gga_msg, nmea_msg))
        {
          ROS_WARN_STREAM("Unable to parse $--GGA from string: " << nmea_msg);
          continue;
        }
        if (gga_msg.checksum != calc_checksum)
        {
          ROS_ERROR_STREAM("Checksum mismatch on NMEA String \"" << nmea_msg << "\" (calc=" << std::hex << calc_checksum
                                                                 << std::fixed << ")");
          continue;
        }
      }
      last_parsed_gga = nmea_msg;

      // gpgga_t gpg = parse_gpgga((char*)nmea_msg.c_str());
      gpgga_t gpg = parseGpggaFromMsg(gga_msg);
      if (gpg.valid)
      {
        lastGpgga = gpg;
        // parseGpggaFromMsg(msg);
        // ROS_ERROR_STREAM("GGA parse lat: " << gpg.latitude << " lon: " << gpg.longitude);
        // TODO - update time from ros::time
        lastGPS = currentGPSTime;
        currentGPSTime = ros::Time::now();
        lastShipLocalX = shipLocalX;
        lastShipLocalY = shipLocalY;
        std::tie(shipLocalX, shipLocalY) = latlon2xy(gpg.latitude, gpg.longitude);

        shipAntennaX = shipLocalX;
        shipAntennaY = shipLocalY;
      }
    }
    else if (nmea_msg.find("VTG") != std::string::npos)
    {
      // Found a VTG, let's check it
      auto vtg_msg = ds_nmea_msgs::Vtg{};
      if (!ds_nmea_msgs::from_string(vtg_msg, nmea_msg))
      {
        ROS_WARN_STREAM("Unable to parse $--VTG from string: " << nmea_msg);
        continue;
      }
      if (vtg_msg.checksum != calc_checksum)
      {
        ROS_ERROR_STREAM("Checksum mismatch on NMEA String \"" << nmea_msg << "\" (calc=" << std::hex << calc_checksum
                                                               << std::fixed << ")");
        continue;
      }
      // okay we've got a good parse, let's assign some variables
      shipCog = vtg_msg.track_degrees_true;
      shipSpeed = vtg_msg.speed_knots;
      shipHeading = vtg_msg.track_degrees_true;
      // spit it back out raw
      m_conn->send(nmea_msg + "\n");
      m_debug_conn->send(nmea_msg + "\n");

      // Check ship speed and referenceSpeed
      if (dp_init && shipSpeed >= MAX_REF_SPEED)
      {
        ROS_ERROR_STREAM_THROTTLE(60,"SHIP SPEED > MAX REF SPEED");
      }
    }
    else
    {
      for (auto it = passthrough_strings.begin(); it != passthrough_strings.end(); ++it)
      {
        if (nmea_msg.find(*it) != std::string::npos)
        {
          // ROS_WARN_STREAM("Found passthru string: "<<nmea_msg);
          m_conn->send(nmea_msg + "\n");
          m_debug_conn->send(nmea_msg + "\n");
        }
        else
        {
          ROS_INFO_STREAM("Ignoring message: " << nmea_msg);
        }
      }
    }
  }
}

void DsDp::on_bridge_in_msg(const ds_core_msgs::RawData& bytes)
{
}

void DsDp::on_gyro_in_msg(const ds_core_msgs::RawData& bytes)
{
  auto stream = std::stringstream(std::string{ std::begin(bytes.data), std::end(bytes.data) });
  auto nmea_msg = std::string{};

  while (std::getline(stream, nmea_msg))
  {
    auto begin_it = nmea_msg.find('$');
    if (begin_it == std::string::npos)
    {
      continue;
    }

    if (begin_it != 0)
    {
      nmea_msg = nmea_msg.substr(begin_it);
    }
    uint8_t calc_checksum = ds_nmea_msgs::calculate_checksum(nmea_msg);

    // Loop over messages types of interest.
    if (nmea_msg.find("HDT") != std::string::npos)
    {
      auto msg = ds_nmea_msgs::Hdt{};
      if (!ds_nmea_msgs::from_string(msg, nmea_msg))
      {
        ROS_WARN_STREAM("Unable to parse $--HDT from string: " << nmea_msg);
        continue;
      }
      if (msg.checksum != calc_checksum)
      {
        ROS_ERROR_STREAM("Checksum mismatch on NMEA String \"" << nmea_msg << "\" (calc=" << std::hex << calc_checksum
                                                               << std::fixed << ")");
        continue;
      }

      shipGyroHdg = msg.heading;
    }
    else
    {
    }
  }
}

void DsDp::on_vfr_in_msg(const ds_core_msgs::RawData& msg)
{
  std::string vfr_string(msg.data.data(), msg.data.data() + msg.data.size());
  // ROS_WARN_STREAM("VFR STRING: " << vfr_string);
  std::vector<std::string> vfr_list;
  boost::split(vfr_list, vfr_string, boost::is_any_of(" "));
  if (vfr_list.size() != 14)
  {
    ROS_ERROR_STREAM("NO VALID VFR");
    return;
  }
  if (vfr_list[0] != "VFR")
  {
    ROS_ERROR_STREAM("COULD NOT FIND VFR, RETURNING");
    return;
  }
  if (vfr_list[5] != "SOLN_GPS0")
  {
    // ROS_ERROR_STREAM("COULD NOT FIND SOLN_GPS, RETURNING");
    return;
  }
  auto lon = std::stod(vfr_list[6]);
  auto lat = std::stod(vfr_list[7]);

  std::stringstream nav_corrected_ss;
  nav_corrected_ss << "$VFRGPGGA"
                   << ",";
  nav_corrected_ss << ds_nmea_msgs::to_nmea_utc_str(msg.header.stamp) << ",";  // 1. Time (UTC)
  nav_corrected_ss << ds_nmea_msgs::to_nmea_lat_string(lat);                   // 2. Lat  3. N/S
  nav_corrected_ss << ds_nmea_msgs::to_nmea_lon_string(lon);                   // 4. Lon  5. E/W
  nav_corrected_ss << "2"
                   << ",";  // 6. GPS Quality indicator (set to GPS, not DGPS)
  nav_corrected_ss << "14"
                   << ",";  // 7. Number of satellites in view, pretend it's sufficient
  nav_corrected_ss << "1.0"
                   << ",";  // 8. Horizontal Dilution of precision
  nav_corrected_ss << "14.300"
                   << ",";  // 9. Antenna altitude above/below mean-sea-level
  nav_corrected_ss << "M"
                   << ",";  // 10. Units of antenna altitude, meters
  nav_corrected_ss << "0.000"
                   << ",";  // 11) Geoidal separation, the difference between the WGS-84 earth ellipsoid and
                            // mean-sea-level (geoid),
                            // "-" means mean-sea-level below ellipsoid
  nav_corrected_ss << "M"
                   << ",";    // 12) Units of geoidal separation, meters
  nav_corrected_ss << "5.0";  // 13) Age of differential GPS data, time in seconds since last SC104 type 1 or 9
                              // update, null field when DGPS is not used
  nav_corrected_ss << "*";    // 14) Differential reference station ID, 0000-1023
  // Now calculate the checksum
  uint16_t checksum = ds_nmea_msgs::calculate_checksum(nav_corrected_ss.str());

  // Append the checksum
  std::string checksum_str = ds_util::int_to_hex<uint16_t>(checksum);
  nav_corrected_ss << checksum_str << "\r\n";  // 15) Checksum
  // ROS_WARN_STREAM("VFR GGA: " << nav_corrected_ss.str());
  lastNavestLat = ds_nmea_msgs::to_nmea_lat_string(lat);
  lastNavestLon = ds_nmea_msgs::to_nmea_lon_string(lon);
  nav_corrected_gps = nav_corrected_ss.str();
  m_vfr_conn->send(nav_corrected_ss.str());
}
void DsDp::on_ui_in_msg(const ds_core_msgs::RawData& bytes)
{
  // These are commands from a GUI operator
  auto msg = std::string{ reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() };

  // ROS_WARN_STREAM("ds_dp UI message: " << msg);

  // on or off
  if (msg.find("DP RDP") != std::string::npos)
  {
    int mode;
    auto n = sscanf(msg.c_str(), "DP RDP %d", &mode);
    if (n == 1)
    {
      if (mode == 0)
      {
        setDPType(NO_DP);
      }
      else if (mode == 1)
      {
        dp_enabled = 1;
        ROS_WARN_STREAM_ONCE("DP ENABLED SET TO " << dp_enabled);
        setDPType(FAUX_GPS);
      }
    }
  }
  else if (msg.find("DP REFSPEED") != std::string::npos)
  {
    double ui_ref_speed;
    auto n = sscanf(msg.c_str(), "DP REFSPEED %lf", &ui_ref_speed);
    if (n == 1)
    {
      if (ui_ref_speed <= MAX_REF_SPEED)
      {
        referenceSpeed = ui_ref_speed;
      }
      else
      {
        ROS_ERROR_STREAM("UI REF SPEED > MAX_REF_SPEED");
        referenceSpeed = MAX_REF_SPEED;
      }
    }
  }

  // xy step
  else if (msg.find("DP XYS") != std::string::npos)
  {
    double x_inc;
    double y_inc;
    const auto n = sscanf(msg.c_str(), "DP XYS %lf %lf", &x_inc, &y_inc);
    if (n == 2)
    {
      incrementGoal(x_inc, y_inc);
    }
    ROS_WARN_STREAM("Adding DP XYS Goal");
  }
  // range and bearing goal
  else if (msg.find("DP RBG") != std::string::npos)
  {
    double range;
    double bearing;
    const auto n = sscanf(msg.c_str(), "DP RBG %lf %lf", &range, &bearing);
    if (n == 2)
    {
      setGoalToRangeBearing(range, bearing);
    }
    ROS_WARN_STREAM("Adding DP RBG Goal");
  }
  // lat lon goal
  else if (msg.find("DP GLL") != std::string::npos)
  {
    double lat;
    double lon;
    const auto n = sscanf(msg.c_str(), "DP GLL %lf %lf", &lon, &lat);
    if (n == 2)
    {
      setGoalToLatLon(lat, lon);
    }

    ROS_WARN_STREAM("Adding DP GLL Goal");
  }
  // goal to ref
  else if (msg.find("DP GTR") != std::string::npos)
  {
    setGoalToRef();
    ROS_WARN_STREAM("Adding Goal to Ref Goal");
  }
  // initialize dp
  else if (msg.find("DP INI") != std::string::npos)
  {
    std_srvs::SetBoolRequest req;
    std_srvs::SetBoolResponse res;
    req.data = true;
    init_cmd(req, res);
    ROS_WARN_STREAM("DP INI Requested");
  }
}

void DsDp::make_ui_update_msg()
{
  // A synchronous status update for the UI
  auto os = std::ostringstream{};

  double goal_lat;
  double goal_lon;
  double ref_lat;
  double ref_lon;
  double anti_ref_lat;
  double anti_ref_lon;
  std::tie(goal_lat, goal_lon) = xy2latlon(goalX, goalY);
  std::tie(ref_lat, ref_lon) = xy2latlon(referenceX, referenceY);
  std::tie(anti_ref_lat, anti_ref_lon) = xy2latlon(antiReferenceX, antiReferenceY);

  os << "DPS " << ((dpType == NO_DP) ? 0 : 1) << " " << std::setprecision(5) << m_ex << " " << std::setprecision(5)
     << m_ey << " " << ((dpType == NO_DP) ? 0 : 1) << " " << reasonableReferenceSpeed << " " << std::fixed
     << std::setprecision(9) << goal_lon << " " << std::fixed << std::setprecision(9) << goal_lat << " " << std::fixed
     << std::setprecision(9) << ref_lat << " " << std::fixed << std::setprecision(9) << ref_lon << " " << shipSpeed
     << " " << shipCog << " " << std::setprecision(5) << actualTimeGoalMin << " " << std::setprecision(5)
     << actualTimeGoalSec << " " << std::setprecision(5) << actualDistGoalMeters << " " << std::fixed
     << std::setprecision(9) << anti_ref_lat << " " << std::fixed << std::setprecision(9) << anti_ref_lon;

  m_ui_conn->send(os.str() + "\n");
}

// ALGORITHM METHODS
void DsDp::setGoal(double x, double y)
{
  goalX = x;
  goalY = y;
}

void DsDp::setGoalToLatLon(double lat, double lon)
{
  std::tie(goalX, goalY) = latlon2xy(lat, lon);
}

void DsDp::setGoalToRef()
{
  goalX = referenceX;
  goalY = referenceY;
}

void DsDp::setGoalToRangeBearing(double inrange, double inbearing)
{
  double range = inrange;
  double bearing = inbearing;
  double dx = sin(bearing * DTOR) * range;
  double dy = cos(bearing * DTOR) * range;

  double newGoalX = shipLocalX + dx;
  double newGoalY = shipLocalY + dy;
  setGoal(newGoalX, newGoalY);
}

void DsDp::setRefToShip()
{
  referenceX = shipLocalX;
  referenceY = shipLocalY;
}

void DsDp::setOrigintoReference()
{
  dpOriginX = referenceX;
  dpOriginY = referenceY;
}

void DsDp::setDPType(eDP_TYPE theDpType)
{
  if ((theDpType >= MIN_DP_TYPE) && (theDpType <= MAX_DP_TYPE))
  {
    dpType = theDpType;
  }
}

void DsDp::incrementGoal(double xIncrement, double yIncrement)
{
  goalX += xIncrement;
  goalY += yIncrement;
}

void DsDp::computeReference()
{
  double dxGR = goalX - referenceX;
  double dyGR = goalY - referenceY;
  double distance = sqrt(dxGR * dxGR + dyGR * dyGR);

  if (distance < 0.001)
  {
    distance = 0.001;
  }

  double dt = (double)(currentGPSTime - lastGPS).toNSec() / 1000000000.0;
  if (dt < 0.01)
  {
    dt = 0.01;
  }
  double xfactor = dxGR / distance;
  double yfactor = dyGR / distance;
  double speed;
  double displacement;
  double refSpeed = KNOTS_TO_MSEC(referenceSpeed);
  double trajectoryTimeConstant = TRAJECTORY_TIME_CONSTANT;

  // No Ramp Up/Down
  //speed = refSpeed;
  //displacement = speed * dpUpdateInterval;

  // Auto Ramp Up/Down
  if (refSpeed > 0.20)
  {  // if going faster than 0.2 knots, change the time constant so as to  slow down sooner
    trajectoryTimeConstant = trajectoryTimeConstant * 1.30;
  }
  // check again, in case we're going really fast--make this a two stage thing
  // Jonathan Howland, 18 october 2015
  if (refSpeed > 0.40)
  {  // if going faster than 0.8 knots, change the time constant so as to  slow down even sooner
     // the effect will be to multiply the time constant by 1.69
    trajectoryTimeConstant = trajectoryTimeConstant * 1.30;
  }

  // 18 October 2015, jch--note that the below two references to the trajectory time constant used the
  // pound-defined number until now, so there was no affect from the above stretching of the time constant

  if (fabs(distance / trajectoryTimeConstant) > refSpeed)
  {
    speed = refSpeed;

    // do not use dt here!  use the update interval.  That is how far we want the ship to move in this time tick.
    // using dt when the gps fix rate is faster than the update interval causes a trajectory speed error
    // displacement = speed * dt;
    displacement = speed * dpUpdateInterval;
  }
  else
  {
    displacement = (1.0 - exp(-dpUpdateInterval / trajectoryTimeConstant)) * distance;
    speed = displacement / dpUpdateInterval;
    referenceSpeed = MSEC_TO_KNOTS(speed);
  }

  referenceX += xfactor * displacement;  // note that xfactor and yfactor are really the sine and cosine of the
                                         // desired displacement
  referenceY += yfactor * displacement;
}

void DsDp::updateRefToGoal()
{
  double dx = goalX - referenceX;
  double dy = goalY - referenceY;
  double distanceToGoal = sqrt(dx * dx + dy * dy);
  double refSpeed = KNOTS_TO_MSEC(referenceSpeed);
  if (refSpeed > 0.000001)
  {
    double timeInSeconds = distanceToGoal / refSpeed;
    currentTimeToGoal = timeInSeconds;
    setTimeToGoal(timeInSeconds);
  }
  else
  {
    // setTimeToGoal(INFINITY);
    setTimeToGoal(999999);
  }
}

void DsDp::setTimeToGoal(double theTime)
{
  if (999999 == theTime)
  {
    std::ostringstream os{};
    os << "reference speed is 0.  Not moving toward goal";
    actualTimeToGoal = os.str();
    typedef std::numeric_limits<double> Info;
    actualTimeGoalMin = Info::quiet_NaN();
    actualTimeGoalSec = Info::quiet_NaN();
    // referenceToGoalLabel->setText("reference speed is 0.  Not moving toward goal");
  }
  else
  {
    int timeInMinutes = (int)(theTime / 60.0);
    int leftoverSeconds = (int)(theTime - (timeInMinutes * 60));
    std::ostringstream os{};
    os << "Time to reach goal: " << timeInMinutes << " min " << leftoverSeconds << " s";
    actualTimeToGoal = os.str();
    actualTimeGoalMin = timeInMinutes;
    actualTimeGoalSec = leftoverSeconds;
    // ROS_WARN_STREAM(os.str());
    //  referenceToGoalLabel->setText("Time to reach goal: " + QString::number(timeInMinutes) + " min " +
    //  QString::number(leftoverSeconds) + " s");
  }
}

// FUTURE NMEA METHODS
gpgga_t DsDp::parseGpggaFromMsg(ds_nmea_msgs::Gga& msg)
{
  // Get the time in UTC
  // boost::posix_time::ptime current_posix_time = ros::Time::now().toBoost();
  // std::string iso_time_str = boost::posix_time::to_iso_extended_string(current_posix_time)
  // ROS_WARN_STREAM("ISO TIME STR: "<<iso_time_str);

  gpgga_t return_gpgga;
  return_gpgga.age = msg.dgps_age;
  return_gpgga.altitude = msg.antenna_alt;
  return_gpgga.geoid_separation = msg.geoid_separation;
  return_gpgga.geoidal_separation = 0.0;
  return_gpgga.hdop = msg.hdop;
  int n_or_s;
  int e_or_w;
  if (msg.latitude_dir == 'S')
  {
    n_or_s = -1;
  }
  else
  {
    n_or_s = 1;
  }
  if (msg.longitude_dir == 'W')
  {
    e_or_w = -1;
  }
  else
  {
    e_or_w = 1;
  }
  return_gpgga.latitude = msg.latitude * n_or_s;
  return_gpgga.longitude = msg.longitude * e_or_w;
  return_gpgga.num_satellites = msg.num_satellites;
  return_gpgga.quality = msg.fix_quality;
  int gpgga_sec = msg.timestamp.sec;
  int gpgga_nsec = msg.timestamp.nsec;
  return_gpgga.utc_time = return_gpgga.utcHours * 3600 + return_gpgga.utcMinutes * 60.0 + return_gpgga.utcSeconds;
  return_gpgga.utcHours = gpgga_sec * 3600;
  return_gpgga.utcMinutes = gpgga_sec * 60;
  return_gpgga.utcSeconds = gpgga_sec;

  // Only gets here if checksum already passed so we know it's valid
  return_gpgga.valid = 1;

  return return_gpgga;
}
// LEGACY NMEA METHODS
gpgga_t DsDp::parse_gpgga(char* input_string)
{
  gpgga_t return_gpgga;
  return_gpgga.valid = 0;
  // first, run through the data and check the checksum
  unsigned short computed_checksum = compute_nmea_checksum(input_string);
  char* checksum_place = strstr(input_string, "*");
  if (checksum_place)
  {
    unsigned int read_checksum;
    int items = sscanf(checksum_place + 1, "%02x", &read_checksum);

    if (items)
    {
      if (read_checksum != computed_checksum)
      {
        return return_gpgga;
      }
      else
      {  // the checksum is valid

        // parse the gpgga
        int utc_hrs, utc_mins, latd, lond, nsat;
        double utc_secs, latmin, lonmin, hdop, alt;
        char le_or_w, ln_or_s, altunits;
        int n_or_s, e_or_w;
        int quality;
        char talker[2];
        if ('N' == input_string[2])  // make it so we can handle a GNGGA just like a GPGGa
        {
          input_string[2] = 'P';
        }

        items = sscanf(input_string, "$%2cGGA,%2d%2d%lf,%2d%lf,%c,%3d%lf,%c,%d,%d,%lf,%lf,%c,", talker, &utc_hrs,
                       &utc_mins, &utc_secs, &latd, &latmin, &ln_or_s, &lond, &lonmin, &le_or_w, &quality, &nsat, &hdop,
                       &alt, &altunits);

        if (items != 15)
        {
          return return_gpgga;
        }
        if (ln_or_s == 'S')
        {
          n_or_s = -1;
        }
        else
        {
          n_or_s = 1;
        }
        if (le_or_w == 'W')
        {
          e_or_w = -1;
        }
        else
        {
          e_or_w = 1;
        }
        return_gpgga.latitude = (latd + latmin / 60.0) * n_or_s;
        return_gpgga.longitude = (lond + lonmin / 60.0) * e_or_w;
        return_gpgga.num_satellites = nsat;
        return_gpgga.hdop = hdop;
        return_gpgga.quality = quality;
        return_gpgga.geoidal_separation = 0.0;
        if ((altunits == 'f') || (altunits == 'F'))
        {
          alt *= METERS_FOOT;
        }
        return_gpgga.altitude = alt;

        return_gpgga.valid = 1;
        return_gpgga.utcHours = utc_hrs;
        return_gpgga.utcMinutes = utc_mins;
        return_gpgga.utcSeconds = utc_secs;
        return_gpgga.utc_time = utc_hrs * 3600 + utc_mins * 60.0 + utc_secs;

        return return_gpgga;
      }
    }
    else
    {
      return return_gpgga;
    }
  }
  else
  {
    return return_gpgga;
  }
}

unsigned short DsDp::compute_nmea_checksum(char* msg)
{
  unsigned short checksum = 0;
  int slen = strlen(msg);
  for (int charplace = 0; charplace < slen; charplace++)
  {
    if (msg[charplace] == '*')
    {
      break;
    }
    switch (msg[charplace])
    {
      case '$':

        break;
      case '*':

        continue;

      default: {
        if (checksum == 0)
        {
          checksum = (unsigned short)msg[charplace];
        }
        else
        {
          checksum = checksum ^ (unsigned short)msg[charplace];
        }
        break;
      }
    }
  }
  return checksum;
}

int DsDp::fmt_gpgga(gpgga_t* g, char* string, char* inTimeString)
{
#define GGA_FMT "$GPGGA,%09.2lf,%02d%09.6lf,%1c,%03d%09.6lf,%1c,%1d,%02d,%.1lf,%05.1lf,M,%05.1lf,M,1.0,0000*"
  int latDeg, lonDeg;
  double latMin, lonMin;

  char latchar;
  char lonchar;

  double useLat = g->latitude;
  double useLon = g->longitude;
  double itime = g->utcSeconds + 100.0 * (double)g->utcMinutes + 10000.0 * (double)g->utcHours;

  latDeg = (int)(fabs(useLat) - fmod(fabs(useLat), 1.0));
  lonDeg = (int)(fabs(useLon) - fmod(fabs(useLon), 1.0));
  latMin = 60.0 * fmod(fabs(useLat), 1.0);
  lonMin = 60.0 * fmod(fabs(useLon), 1.0);
  if (useLat > 0)
  {
    latchar = 'N';
  }
  else
  {
    latchar = 'S';
  }
  if (useLon < 0)
  {
    lonchar = 'W';
  }
  else
  {
    lonchar = 'E';
  }

  int len = sprintf(string, GGA_FMT, itime, latDeg, latMin, latchar, lonDeg, lonMin, lonchar, g->quality,
                    g->num_satellites, g->hdop, g->altitude, g->geoidal_separation);

  unsigned short computedChecksum = compute_nmea_checksum(string);
  len += sprintf(&(string[len]), "%02X\r\n", computedChecksum);
  return len;
}

}  // namespace ds_dp
