/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_nav/ds_nav.h"
#include "ds_nav_private.h"

namespace ds_nav
{
DsNav::DsNav() : DsProcess(), d_ptr_(std::unique_ptr<DsNavPrivate>(new DsNavPrivate))
{
  startTf(true);
}

DsNav::DsNav(int argc, char* argv[], const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<DsNavPrivate>(new DsNavPrivate))
{
  startTf(true);
}

DsNav::~DsNav() = default;

tf2_ros::Buffer& DsNav::getTfBuffer(void)
{
  return tfBuffer_;
}

void DsNav::startTf(bool spin_thread)
{
  // Set up tf and start listening
  auto nh = nodeHandle();
  tfListener_.reset(new tf2_ros::TransformListener(tfBuffer_, nh, spin_thread));
}

void DsNav::stopTf()
{
  // Destroys the tf listener object and stops receiving incoming tf messages
  tfListener_.reset(nullptr);
}

double DsNav::dtor(double degrees)
{
  return (degrees * M_PI / 180.0);
}

double DsNav::rtod(double radians)
{
  return (radians * 180.0 / M_PI);
}

void DsNav::sendTf(std::string child_id, std::string frame_id, double x, double y, double z,
                   geometry_msgs::Quaternion q, ros::Time tfTime)
{
  geometry_msgs::TransformStamped transformStamped;

  transformStamped.header.stamp = tfTime;
  transformStamped.header.frame_id = frame_id;
  transformStamped.child_frame_id = child_id;
  transformStamped.transform.translation.x = x;
  transformStamped.transform.translation.y = y;
  transformStamped.transform.translation.z = z;
  transformStamped.transform.rotation.x = q.x;
  transformStamped.transform.rotation.y = q.y;
  transformStamped.transform.rotation.z = q.z;
  transformStamped.transform.rotation.w = q.w;

  br_.sendTransform(transformStamped);
}

void DsNav::sendPose(ros::Publisher pub, std::string frame_id, double x, double y, double z,
                     geometry_msgs::Quaternion q, ros::Time poseTime)
{
  geometry_msgs::PoseStamped poseStamped;

  poseStamped.pose.position.x = x;
  poseStamped.pose.position.y = y;
  poseStamped.pose.position.z = z;
  poseStamped.pose.orientation.x = q.x;
  poseStamped.pose.orientation.y = q.y;
  poseStamped.pose.orientation.z = q.z;
  poseStamped.pose.orientation.w = q.w;
  poseStamped.header.stamp = poseTime;
  poseStamped.header.frame_id = frame_id;

  pub.publish(poseStamped);
}

void DsNav::setOrigin(double lat, double lon)
{
  origin_ = Origin(lat, lon);
}

std::tuple<double, double> DsNav::xy2latlon(double x, double y)
{
  GeographicLib::Geocentric earth(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
  GeographicLib::LocalCartesian proj(origin_.lat(), origin_.lon(), 0, earth);
  double lat, lon, h;
  proj.Reverse(x, y, 0, lat, lon, h);
  return std::make_pair(lat, lon);
}

std::tuple<double, double> DsNav::latlon2xy(double lat, double lon)
{
  GeographicLib::Geocentric earth(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
  GeographicLib::LocalCartesian proj(origin_.lat(), origin_.lon(), 0, earth);
  double x, y, z;
  proj.Forward(lat, lon, 0, x, y, z);
  return std::make_tuple(x, y);
}

std::tuple<double, double> DsNav::directGeodesic(double lat, double lon, double depth, double heading, double distance)
{
  // We should reduce the WGS84_a, equatorial radius, by a function of depth
  GeographicLib::Geodesic geod(GeographicLib::Constants::WGS84_a(), GeographicLib::Constants::WGS84_f());
  double lat2, lon2;
  geod.Direct(lat, lon, heading, distance, lat2, lon2);
  return std::make_tuple(lat2, lon2);
}

void DsNav::setTransform(geometry_msgs::TransformStamped in, std::string authority)
{
  tfBuffer_.setTransform(in, authority);
}
}
