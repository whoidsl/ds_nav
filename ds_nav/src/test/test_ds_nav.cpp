/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_nav/ds_nav.h"
#include <gtest/gtest.h>

// This allows us to call protected functions
class DsNavFixture : public ds_nav::DsNav, public testing::Test
{
protected:
  double lat0 = 48.0;
  double lon0 = 2.0;

  void SetUp() override
  {
    setup();

    setOrigin(lat0, lon0);
  }
};

TEST_F(DsNavFixture, checkXy2LatLon)
{
  double x = 0;
  double y = 0;
  double lat;
  double lon;
  std::tie(lat, lon) = xy2latlon(x, y);
  ASSERT_FLOAT_EQ(lat, lat0);
  ASSERT_FLOAT_EQ(lon, lon0);

  // echo 1000 1000 0 | CartConvert -r -l 48 2 0
  // 48.01798636593 2.01340493099 0.392183
  x = 1000;
  y = 2000;
  std::tie(lat, lon) = xy2latlon(x, y);
  ASSERT_NEAR(lat, 48.01798636593, 0.00001);
  ASSERT_NEAR(lon, 2.01340260073, 0.00001);
}

TEST_F(DsNavFixture, checkLatLon2Xy)
{
  double x;
  double y;
  double lat = 48.0;
  double lon = 2.0;
  std::tie(x, y) = latlon2xy(lat, lon);
  ASSERT_FLOAT_EQ(x, 0);
  ASSERT_FLOAT_EQ(y, 0);
}

// Run all the tests that were declared with TEST() or TEST_F()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "ds_nav");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
};
