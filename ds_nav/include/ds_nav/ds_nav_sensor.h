/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_NAV_SENSOR_H
#define DS_NAV_SENSOR_H

#include "boost/circular_buffer.hpp"
#include "ros/ros.h"
#include "boost/optional.hpp"

namespace ds_nav
{
template <class T>
class DsNavSensor
{
public:
  /// @brief Default constructor, constructs a DsNavSensor with a history length of 2 (last/previous)
  DsNavSensor();

  /// @brief Constructs a DsNavSensor with a specified history length
  DsNavSensor(int history);

  /// @brief Destructs a DsNavSensor
  virtual ~DsNavSensor();

  /// @ Overload to the operator () that returns the most recent data for this sensor
  T operator()();

  /// @brief inserts a sensor message input into the circular buffer
  void push_back(T input, ros::Time time = ros::Time::now());

  /// @brief set the SO3 transformation FROM this sensor frame TO the base_link frame
  void setTransform(Eigen::Affine3d input);

  /// @brief returns a reference to the SO3 transformation FROM this sensor frame TO the base_link frame
  Eigen::Affine3d& getTransform(void);

  /// @brief gets the rotation component of the transform as 3x3 matrix
  Eigen::Matrix3d rotation(void);

  /// @brief gets the translation component of the transform
  Eigen::Vector3d translation(void);

  /// @brief gets the rotation component of the transform as quaternion
  Eigen::Quaterniond quaternion(void);

  /// @brief returns the last sensor object stored
  T getLast(void);

  /// @brief get the ros time of the last sensor data
  ros::Time getLastTime(void);

  /// @brief returns the previous sensor object stored
  T getPrevious(void);

  /// @brief returns the age of the last measurements. Default argument is age to when the function is called
  ros::Duration age(ros::Time to = ros::Time::now());

  /// @brief returns the deltat between the last two measurements
  ros::Duration dt();

  /// @brief returns whether the circular buffer is full or not
  bool full(void);

  /// @brief returns whether the circular buffer is empty or not
  bool empty(void);

private:
  /// @brief A circular buffer of navigation sensor messages objects
  boost::circular_buffer<T> object_;

  /// @brief Time associated with reception of the message objects
  boost::circular_buffer<ros::Time> object_time_;

  /// @brief object that stores the most recent SO3 transformation FROM this sensor frame TO the base_link frame
  Eigen::Affine3d transform_;
};

template <class T>
DsNavSensor<T>::DsNavSensor() : object_(2), object_time_(2){};

template <class T>
DsNavSensor<T>::DsNavSensor(int history) : object_(history), object_time_(history){};

template <class T>
DsNavSensor<T>::~DsNavSensor(){};

template <class T>
T DsNavSensor<T>::operator()()
{
  return object_.back();
};

template <class T>
Eigen::Matrix3d DsNavSensor<T>::rotation(void)
{
  return transform_.rotation();
};

template <class T>
Eigen::Vector3d DsNavSensor<T>::translation(void)
{
  return transform_.translation();
};

template <class T>
Eigen::Quaterniond DsNavSensor<T>::quaternion(void)
{
  return Eigen::Quaterniond(transform_.rotation());
};

template <class T>
void DsNavSensor<T>::push_back(T input, ros::Time time)
{
  object_.push_back(input);
  object_time_.push_back(time);
};

template <class T>
T DsNavSensor<T>::getLast(void)
{
  return object_.back();
};

template <class T>
ros::Time DsNavSensor<T>::getLastTime(void)
{
  return object_time_.back();
}

template <class T>
T DsNavSensor<T>::getPrevious(void)
{
  return object_.front();
};

template <class T>
void DsNavSensor<T>::setTransform(Eigen::Affine3d input)
{
  transform_ = input;
}

template <class T>
Eigen::Affine3d& DsNavSensor<T>::getTransform(void)
{
  return transform_;
}

template <class T>
ros::Duration DsNavSensor<T>::age(ros::Time to)
{
  return (to - object_time_.back());
}

template <class T>
ros::Duration DsNavSensor<T>::dt(void)
{
  return (object_time_.back() - object_time_.front());
}

template <class T>
bool DsNavSensor<T>::full(void)
{
  return object_.full();
}

template <class T>
bool DsNavSensor<T>::empty(void)
{
  return object_.empty();
}
};

#endif
