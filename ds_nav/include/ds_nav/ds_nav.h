/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_NAV_H
#define DS_NAV_H

#include "ds_base/ds_process.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
#include <tf2_ros/transform_broadcaster.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Quaternion.h"

#include <GeographicLib/Geocentric.hpp>
#include <GeographicLib/LocalCartesian.hpp>
#include <GeographicLib/Geodesic.hpp>

namespace ds_nav
{
class Origin
{
public:
  Origin() : Origin(0, 0){};
  Origin(double lat, double lon)
  {
    lat_ = lat;
    lon_ = lon;
  };
  virtual ~Origin() = default;

  double lat(void)
  {
    return lat_;
  };
  double lon(void)
  {
    return lon_;
  };

private:
  double lat_;
  double lon_;
};

struct DsNavPrivate;

class DsNav : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(DsNav)

public:
  explicit DsNav();
  DsNav(int argc, char* argv[], const std::string& name);

  ~DsNav() override;

  DS_DISABLE_COPY(DsNav)

  /// @brief returns the radians representation of the angle given in degrees
  double dtor(double degrees);

  /// @brief returns the degrees representation of the angle given in radians
  double rtod(double radians);

  /// @brief send a Tf transform from child_id frame to frame_id frame. If time is not provided, it will use the
  /// function invocation time
  void sendTf(std::string child_id, std::string frame_id, double x, double y, double z, geometry_msgs::Quaternion q,
              ros::Time tfTime = ros::Time::now());

  /// @brief send a Pose message of frame frame_id using the publisher provided. If time is not provided, it will use
  /// the function invocation time
  void sendPose(ros::Publisher pub, std::string frame_id, double x, double y, double z, geometry_msgs::Quaternion q,
                ros::Time poseTime = ros::Time::now());

  /// @brief sets the origin class
  void setOrigin(double lat, double lon);

  /// @brief returns a tuple with latitude and longitude given x and y
  std::tuple<double, double> xy2latlon(double x, double y);

  /// @brief returns a tuple with x and y given latitude and longitude
  std::tuple<double, double> latlon2xy(double x, double y);

  /// @brief returns a tuple with geodetically integrated velocities at depth
  std::tuple<double, double> directGeodesic(double lat, double lon, double depth, double heading, double distance);

  void startTf(bool spin_thread);

  void stopTf();

  /// @brief Store a transform in the tf2 buffer from a "manual" authority
  void setTransform(geometry_msgs::TransformStamped in, std::string authority);

  /// @brief Get a reference to the private tf2 transforms buffer
  tf2_ros::Buffer& getTfBuffer(void);

protected:
private:
  std::unique_ptr<DsNavPrivate> d_ptr_;

  tf2_ros::TransformBroadcaster br_;

  Origin origin_;

  // Tf2 buffers, listeners
  tf2_ros::Buffer tfBuffer_;
  std::unique_ptr<tf2_ros::TransformListener> tfListener_;
};
}

#endif
