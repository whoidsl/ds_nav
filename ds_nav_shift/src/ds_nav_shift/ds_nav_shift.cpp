/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

#include "ds_nav_shift/ds_nav_shift.h"

#include <GeographicLib/Constants.hpp>
#include <GeographicLib/TransverseMercator.hpp>

namespace ds_nav {


NavShift::NavShift() : ds_base::DsProcess() {
  // do nothing
}

NavShift::NavShift(int argc, char* argv[], const std::string& name) : ds_base::DsProcess(argc, argv, name) {
  // do nothing
}

NavShift::~NavShift() = default;

ds_nav_msgs::AggregatedState NavShift::shiftAgg(const ds_nav_msgs::Shift& shift,
                                               const ds_nav_msgs::AggregatedState& agg) {
  ds_nav_msgs::AggregatedState ret(agg); // copy

  double shift_sign = 1.0;
  if (shift.shift_like_mc) {
    shift_sign = -1.0;
  }

  if (ret.northing.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
    ret.northing.value += (shift_sign*shift.shift_northing);
  }

  if (ret.easting.valid == ds_nav_msgs::FlaggedDouble::VALUE_VALID) {
    ret.easting.value += (shift_sign*shift.shift_easting);
  }

  return ret;
}

ds_nav_msgs::NavState NavShift::shiftState(const ds_nav_msgs::Shift& shift,
                                          const ds_nav_msgs::NavState& state) {
  ds_nav_msgs::NavState ret(state); // copy

  double shift_sign = 1.0;
  if (shift.shift_like_mc) {
    shift_sign = -1.0;
  }

  // projected coordinates are easy
  if (!std::isnan(ret.northing)) {
    ret.northing += (shift_sign*shift.shift_northing);
  }

  if (!std::isnan(ret.easting)) {
    ret.easting += (shift_sign*shift.shift_easting);
  }

  //
  // Note: the USBL transforms typically use a LocalCartiesian coordinate frame.
  // That is essentially a rectilinear frame centered at a point on the surface of the
  // ellipsoid and with the axes arranges so that they are tangent to the ellipsoid.
  //
  // While ideal for USBL, this _isn't_ a projection.  Using it for shifts could
  // introduce errors in depth as the shift gets large as the coordinate frame
  // does not follow the curvature of the Earth.  Instead, therefore, we use
  // a projection that DOES follow the curvature of the Earth (or, actually, the ellipsoid,
  // but close enough for practical matters).

  // lat/lon requires a little more special sauce
  if (! (std::isnan(ret.lon) || std::isnan(ret.lat) )) {
    GeographicLib::TransverseMercator tmerc(GeographicLib::Constants::WGS84_a(),
                                               GeographicLib::Constants::WGS84_f(), 1.0);

    // transform into projected coordinates
    double east, north;
    double lon0 = ret.lon;
    tmerc.Forward(lon0, ret.lat, ret.lon, east, north);

    // apply shift
    east += (shift_sign*shift.shift_easting);
    north += (shift_sign*shift.shift_northing);

    // transform back into lat/lon
    tmerc.Reverse(lon0, east, north, ret.lat, ret.lon);
  }

  return ret;
}

void NavShift::setupParameters() {
  ros::NodeHandle nh = nodeHandle();

  current_shift.shift_easting = 0;
  current_shift.shift_northing = 0;
  nh.getParam(ros::this_node::getName() + "/init_shift_easting", current_shift.shift_easting);
  nh.getParam(ros::this_node::getName() + "/init_shift_northing", current_shift.shift_northing);

  ROS_INFO_STREAM("Setting initial shift to NE: " <<current_shift.shift_northing
                                                  <<" " <<current_shift.shift_easting);

  double param_period = 15.0;
  nh.getParam(ros::this_node::getName() + "/shift_publish_period", param_period);
  shift_pub_period = ros::Duration(param_period);
}

void NavShift::setupSubscriptions() {
  ros::NodeHandle nh = nodeHandle();

  sub_shift = nh.subscribe("new_shift", 10, &NavShift::_on_shift_msg, this);
  sub_agg = nh.subscribe("navagg", 10, &NavShift::_on_navagg_msg, this);
  sub_state = nh.subscribe("navstate", 10, &NavShift::_on_navstate_msg, this);
}

void NavShift::setupPublishers() {
  ros::NodeHandle nh = nodeHandle();

  pub_current_shift = nh.advertise<ds_nav_msgs::Shift>("current_shift", 10, true);
  pub_shifted_agg = nh.advertise<ds_nav_msgs::AggregatedState>("shifted_navagg", 10, true);
  pub_shifted_state = nh.advertise<ds_nav_msgs::NavState>("shifted_navstate", 10, true);
}

void NavShift::setupTimers() {
  ros::NodeHandle nh = nodeHandle();

  shift_timer = nh.createTimer(shift_pub_period, &NavShift::_on_timer_evt, this);
}

void NavShift::_on_shift_msg(const ds_nav_msgs::Shift& msg) {
  current_shift = msg;
  pub_current_shift.publish(current_shift);
}

void NavShift::_on_navagg_msg(const ds_nav_msgs::AggregatedState& msg) {
  pub_shifted_agg.publish(shiftAgg(current_shift, msg));
}

void NavShift::_on_navstate_msg(const ds_nav_msgs::NavState& msg) {
  pub_shifted_state.publish(shiftState(current_shift, msg));
}

void NavShift::_on_timer_evt(const ros::TimerEvent& evt) {
  pub_current_shift.publish(current_shift);
}

} // namespace ds_nav
