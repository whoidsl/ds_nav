/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

#include <gtest/gtest.h>
#include <ds_nav_shift/ds_nav_shift.h>

TEST(NavShiftTest, navagg_valid) {
  ds_nav_msgs::AggregatedState state;
  state.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state.northing.value = 12.3;
  state.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state.easting.value = 45.6;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftAgg(shift, state);

  EXPECT_EQ(ds_nav_msgs::FlaggedDouble::VALUE_VALID, shifted_state.northing.valid);
  EXPECT_EQ(112.3, shifted_state.northing.value);
  EXPECT_EQ(ds_nav_msgs::FlaggedDouble::VALUE_VALID, shifted_state.easting.valid);
  EXPECT_EQ(245.6, shifted_state.easting.value);
}

TEST(NavShiftTest, navagg_invalid) {
  ds_nav_msgs::AggregatedState state;
  state.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state.northing.value = 12.3;
  state.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state.easting.value = 45.6;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftAgg(shift, state);

  EXPECT_DOUBLE_EQ(ds_nav_msgs::FlaggedDouble::VALUE_INVALID, shifted_state.northing.valid);
  EXPECT_DOUBLE_EQ(12.3, shifted_state.northing.value);
  EXPECT_DOUBLE_EQ(ds_nav_msgs::FlaggedDouble::VALUE_INVALID, shifted_state.easting.valid);
  EXPECT_DOUBLE_EQ(45.6, shifted_state.easting.value);
}

TEST(NavShiftTest, navstate_valid) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = 12.3;
  state.lon = 23.4;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(112.3, shifted_state.northing);
  EXPECT_DOUBLE_EQ(245.6, shifted_state.easting);
  EXPECT_DOUBLE_EQ(12.300903951172112, shifted_state.lat);
  EXPECT_DOUBLE_EQ(23.401838567025475, shifted_state.lon);
}

TEST(NavShiftTest, navstate_invalid_ne) {
  ds_nav_msgs::NavState state;
  state.northing = std::numeric_limits<double>::quiet_NaN();
  state.easting = std::numeric_limits<double>::quiet_NaN();
  state.lat = 12.3;
  state.lon = 23.4;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_TRUE(std::isnan(shifted_state.northing));
  EXPECT_TRUE(std::isnan(shifted_state.easting));
  EXPECT_DOUBLE_EQ(12.300903951172112, shifted_state.lat);
  EXPECT_DOUBLE_EQ(23.401838567025475, shifted_state.lon);
}
TEST(NavShiftTest, navstate_invalid_ll) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = std::numeric_limits<double>::quiet_NaN();
  state.lon = std::numeric_limits<double>::quiet_NaN();

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(112.3, shifted_state.northing);
  EXPECT_DOUBLE_EQ(245.6, shifted_state.easting);
  EXPECT_TRUE(std::isnan(shifted_state.lat));
  EXPECT_TRUE(std::isnan(shifted_state.lon));
}

TEST(NavShiftTest, navstate_valid_highlat) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = 80.0;
  state.lon = 23.4;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(112.3, shifted_state.northing);
  EXPECT_DOUBLE_EQ(245.6, shifted_state.easting);
  EXPECT_DOUBLE_EQ(80.0008954175475, shifted_state.lat);
  EXPECT_DOUBLE_EQ(23.410313655317896, shifted_state.lon);
}

TEST(NavShiftTest, navstate_valid_dateline) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = 12.3;
  state.lon = 179.999999;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = false;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(112.3, shifted_state.northing);
  EXPECT_DOUBLE_EQ(245.6, shifted_state.easting);
  EXPECT_DOUBLE_EQ(12.300903951172112, shifted_state.lat);
  EXPECT_DOUBLE_EQ(-179.99816243297445, shifted_state.lon);
}

TEST(NavShiftTest, navagg_valid_mclike) {
  ds_nav_msgs::AggregatedState state;
  state.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state.northing.value = 12.3;
  state.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  state.easting.value = 45.6;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = true;

  auto shifted_state = ds_nav::NavShift::shiftAgg(shift, state);

  EXPECT_EQ(ds_nav_msgs::FlaggedDouble::VALUE_VALID, shifted_state.northing.valid);
  EXPECT_EQ(12.3-100, shifted_state.northing.value);
  EXPECT_EQ(ds_nav_msgs::FlaggedDouble::VALUE_VALID, shifted_state.easting.valid);
  EXPECT_EQ(45.6-200, shifted_state.easting.value);
}

TEST(NavShiftTest, navstate_valid_mclike) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = 12.3;
  state.lon = 23.4;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = true;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(12.3-100, shifted_state.northing);
  EXPECT_DOUBLE_EQ(45.6-200, shifted_state.easting);
  EXPECT_NEAR(12.299096036409502, shifted_state.lat, 1.0e-9);
  EXPECT_NEAR(23.398161445542787, shifted_state.lon, 1.0e-9);
}

TEST(NavShiftTest, navstate_valid_highlat_mclike) {
  ds_nav_msgs::NavState state;
  state.northing = 12.3;
  state.easting = 45.6;
  state.lat = 80.0;
  state.lon = 23.4;

  ds_nav_msgs::Shift shift;

  shift.shift_northing = 100;
  shift.shift_easting = 200;
  shift.shift_like_mc = true;

  auto shifted_state = ds_nav::NavShift::shiftState(shift, state);

  EXPECT_DOUBLE_EQ(12.3-100, shifted_state.northing);
  EXPECT_DOUBLE_EQ(45.6-200, shifted_state.easting);
  EXPECT_NEAR(79.99910426491012, shifted_state.lat, 1.0e-9);
  EXPECT_NEAR(23.389688172685794, shifted_state.lon, 1.0e-9);
}

int main(int argc, char* argv[]) {
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}

