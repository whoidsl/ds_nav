/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 9/27/19.
//

#ifndef DS_NAV_SHIFT_H_
#define DS_NAV_SHIFT_H_

#include <ds_base/ds_process.h>
#include <ds_nav_msgs/Shift.h>
#include <ds_nav_msgs/NavState.h>
#include <ds_nav_msgs/AggregatedState.h>

namespace ds_nav {

class NavShift : public ds_base::DsProcess {

 DS_DISABLE_COPY(NavShift);

 public:
  explicit NavShift();
  NavShift(int argc, char* argv[], const std::string& name);
  ~NavShift() override;

  static ds_nav_msgs::AggregatedState shiftAgg(const ds_nav_msgs::Shift& shift,
                                               const ds_nav_msgs::AggregatedState& agg);

  static ds_nav_msgs::NavState shiftState(const ds_nav_msgs::Shift& shift,
                                          const ds_nav_msgs::NavState& state);

 protected:
  void setupParameters() override;
  void setupSubscriptions() override;
  void setupPublishers() override;
  void setupTimers() override;

  void _on_shift_msg(const ds_nav_msgs::Shift& msg);
  void _on_navagg_msg(const ds_nav_msgs::AggregatedState& msg);
  void _on_navstate_msg(const ds_nav_msgs::NavState& msg);
  void _on_timer_evt(const ros::TimerEvent& evt);

 private:
  ds_nav_msgs::Shift current_shift;

  ros::Publisher pub_current_shift;
  ros::Publisher pub_shifted_agg;
  ros::Publisher pub_shifted_state;

  ros::Subscriber sub_shift;
  ros::Subscriber sub_agg;
  ros::Subscriber sub_state;

  ros::Timer shift_timer;
  ros::Duration shift_pub_period;
};

} // namespace ds_nav

#endif // DS_NAV_SHIFT_H_
