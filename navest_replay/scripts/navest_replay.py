#!/usr/bin/env python
# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Originally written 5-9 Jan 2018 by Theo Guerin
#                                    theocguerin@gmail.com
# SS - modified to better fit our pipeline, changes logged in git history

import scipy.io as sio
import os
from datetime import datetime
import sys
import time
import argparse
import re
import rospy
from ds_sensor_msgs.msg import DepthPressure, Ins, Dvl, SoundSpeed
from rosgraph_msgs.msg import Clock
import tf
import math

class MatFile(object):
    def __init__(self):
        pass

    # Adapted from multibeam-processing repo file "mb_lib.py":
    def load_navsci_matlab(self, fname):
        """Load .mat file from nav-sci"""
        try:
            self.mat = sio.loadmat(fname)
            return self.mat
        except IOError, e:
            print(e)
        sys.exit()

    def get_mat_data(self, data_type, get_dvl_array=False):
        try:
            if not get_dvl_array:
                mat_data = self.mat[self.file_type][data_type][0][0][self.sample_count][0]
            else:
                mat_data = self.mat[self.file_type][data_type][0][0][self.sample_count]
            return mat_data
        except IndexError as e:
            #print 'Index Error:', e
            print 'Reached end of a .mat data file. NavEst data replay terminated.'
            sys.exit()

    def get_time(self):
        return self.get_mat_data('t')


class DefFile(MatFile):
    def __init__(self, filename):
        self.filename = filename
        super(DefFile, self).__init__()
        self.mat = self.load_navsci_matlab(self.filename)
        self.file_type = 'def'
        self.data_types = ['depth', 'psi']
        self.sample_count = 0
        self.init_ros_publisher()

    def init_ros_publisher(self):
        self.pub_dp = rospy.Publisher('/sentry/sensors/paro/depth',
                                            DepthPressure, queue_size=1)

    def publish_ros_object(self, t):
        msg = DepthPressure()
        secs, nsecs = secs_and_nsecs(t)
        data_list = []
        for i in self.data_types:
            data_list.append(self.get_mat_data(i))
        msg.depth = data_list[0]
        msg.pressure = data_list[1]
        msg.ds_header.io_time.secs = secs
        msg.ds_header.io_time.nsecs = nsecs
        msg.header.stamp.secs = secs
        msg.header.stamp.nsecs = nsecs
        self.pub_dp.publish(msg)

class HsvpFile(MatFile):
    def __init__(self, filename):
        self.filename = filename
        super(HsvpFile, self).__init__()
        self.mat = self.load_navsci_matlab(self.filename)
        self.file_type = 'hsvp'
        self.data_types = ['ss']
        self.sample_count = 0
        self.init_ros_publisher()

    def init_ros_publisher(self):
        self.pub_hsvp = rospy.Publisher('/sentry/sensors/resonsvp70/sound_speed',
                                            SoundSpeed, queue_size=1)

    def publish_ros_object(self, t):
        msg = SoundSpeed()
        secs, nsecs = secs_and_nsecs(t)
        data_list = []
        for i in self.data_types:
            data_list.append(self.get_mat_data(i))
        msg.speed = data_list[0]
        msg.ds_header.io_time.secs = secs
        msg.ds_header.io_time.nsecs = nsecs
        msg.header.stamp.secs = secs
        msg.header.stamp.nsecs = nsecs
        #self.pub_hsvp.publish(msg)


class GvxFile(MatFile):
    def __init__(self, filename):
        self.filename = filename
        super(GvxFile, self).__init__()
        self.mat = self.load_navsci_matlab(self.filename)
        self.file_type = 'gvx'
        self.data_types = ['roll', 'pitch', 'heading']
        self.sample_count = 0
        self.init_ros_publisher()

    def init_ros_publisher(self):
        self.pub_ins = rospy.Publisher('/sentry/sensors/phins/ins',
                                            Ins, queue_size=1)

    def publish_ros_object(self, t):
        msg = Ins()
        secs, nsecs = secs_and_nsecs(t)
        data_list = []
        for i in self.data_types:
            data_list.append(self.get_mat_data(i))
        msg.roll = math.radians(data_list[0])
        msg.pitch = math.radians(data_list[1])
        msg.heading = math.radians(data_list[2])
        # SS - add quaternion following H-P-R rotation sequence, from Phins frame to E-N-U
        xaxis, yaxis, zaxis = (1, 0, 0), (0, 1, 0), (0, 0, 1)
        qz = tf.transformations.quaternion_about_axis(-msg.heading + (math.pi / 2.0), zaxis)
        qy = tf.transformations.quaternion_about_axis(msg.pitch, yaxis)
        qx = tf.transformations.quaternion_about_axis(msg.roll, xaxis)
        # q is equivalent to qx*qy*qz, which corresponds to H->P->R rotation
        q = tf.transformations.quaternion_multiply(qy, qz)
        q = tf.transformations.quaternion_multiply(qx, q)
        msg.orientation.x = q[0]
        msg.orientation.y = q[1]
        msg.orientation.z = q[2]
        msg.orientation.w = q[3]
        msg.ds_header.io_time.secs = secs
        msg.ds_header.io_time.nsecs = nsecs
        msg.header.stamp.secs = secs
        msg.header.stamp.nsecs = nsecs
        self.pub_ins.publish(msg)


class DvlFile(MatFile):
    def __init__(self, filename):
        self.filename = filename
        super(DvlFile, self).__init__()
        self.mat = self.load_navsci_matlab(self.filename)
        self.file_type = 'dvl'
        #TODO: Consider changing/adding data to retrieve from .mat file:
        # SS - added good bottom beams in this ping
        # SS - added dvl RTC time t
        self.data_types = ['sound_speed', 'bottom_vel', 'good_bottom_beams_in_this_ping', 't']
        self.sample_count = 0
        self.init_ros_publisher()

    def init_ros_publisher(self):
        self.pub_dvl = rospy.Publisher('/sentry/sensors/rdi300/dvl',
                                            Dvl, queue_size=1)

    def publish_ros_object(self, t):
        msg = Dvl()
        secs, nsecs = secs_and_nsecs(t)
        data_list = []
        for i in self.data_types:
            if i == 'bottom_vel':
                get_array = True
            else:
                get_array = False
            data_list.append(self.get_mat_data(i, get_dvl_array=get_array))
        # SS - hardcoded SS to 1500, the matlab dvl structure does not store the dvl sound speed setting but the actual sound speed used
        #msg.speed_sound = data_list[0]
        msg.speed_sound = 1500.0
        # Set the negated bottom_vel numpy.ndarray as raw_velocity:
        # TODO: Check that this negation is correct
        # SS - converted data back into RDi instrument frame with stationary bottom and moving dvl
        msg.raw_velocity[0] = data_list[1][1]
        msg.raw_velocity[1] = data_list[1][0]
        msg.raw_velocity[2] = -data_list[1][2]
        msg.raw_velocity[3] = data_list[1][3]
        msg.velocity.x = msg.raw_velocity[0]
        msg.velocity.y = msg.raw_velocity[1]
        msg.velocity.z = msg.raw_velocity[2]
        msg.coordinate_mode = msg.DVL_COORD_INSTRUMENT
        # SS - add good bottom beams
        msg.num_good_beams = data_list[2]
        # SS - add dvl RTC time t
        msg.dvl_time = data_list[3]
        # Set velocity mode:
        msg.velocity_mode = msg.DVL_MODE_BOTTOM
        msg.ds_header.io_time.secs = secs
        msg.ds_header.io_time.nsecs = nsecs
        msg.header.stamp.secs = secs
        msg.header.stamp.nsecs = nsecs
        self.pub_dvl.publish(msg)

class DvzFile(MatFile):
    def __init__(self, filename):
        self.filename = filename
        super(DvzFile, self).__init__()
        self.mat = self.load_navsci_matlab(self.filename)
        self.file_type = 'dvz'
        self.data_types = ['x', 'y', 'z', 'roll', 'pitch', 'heading']
        self.sample_count = 0
        self.br_dvz = tf.TransformBroadcaster()

    def publish_ros_object(self, t):
        data_list = []
        for i in self.data_types:
            data_list.append(self.get_mat_data(i))
        # Convert rotation from degrees to radians:
        roll = math.radians(data_list[3])
        pitch = math.radians(data_list[4])
        heading = math.radians(data_list[5])
        #self.br_dvz = tf.TransformBroadcaster()
        # TODO: Check that the rotations are correct
        #self.br_dvz.sendTransform(
        #                (data_list[0], data_list[1], -data_list[2]),
        #                tf.transformations.quaternion_from_euler(roll,
        #                                                         pitch,
        #                                                         heading),
        #                rospy.Time.from_sec(t),
        #                'base_link',
        #                'odom_dvz')


# TODO?: Create additional specific .mat file classes

def run_simulation(mat_files, sim_speed_factor, start_time):
    ts_list = []
    # Get initial times:
    for i in mat_files:
        ts_list.append(i.get_time())
    # Next sim/sample time is the minimum/nearest time across all .mat files:
    ts = min(ts_list)
    if args.clock:
        # Initialize clock publisher. Note that the /use_sim_time ROS parameter
        # needs to be set to true in order to be able to run other ROS nodes
        # with the ROS simulation clock:
        pub_clock = rospy.Publisher('/clock', Clock, queue_size=1)
    # Initial wall datetime:
    tw = time.time()
    while not rospy.is_shutdown():
        next_ts_list = []
        for num, i in enumerate(mat_files):
            t = ts_list[num]
            # TODO: Consider implementing a better way to track which
            # minimum time corresponds to which mat object.
            if t == ts:
                current_mat = i
                # Temporarily increment sample count by 1
                # in order to obtain next time sample:
                current_mat.sample_count += 1
                # TODO: Consider making it so that if one file runs out of data,
                # the simulation continues running with the remaining files'
                # data rather than sys.exit()-ing
                next_ts_list.append(current_mat.get_time())
                current_mat.sample_count -= 1
            else:
                next_ts_list.append(t)
        next_ts = min(next_ts_list)
        delta = next_ts - ts
        delta_delay = delta / sim_speed_factor
        # TODO: Consider implementing a faster/smarter, more efficient time
        # search algorithm for starting data replay based on start_time
        # If no start_time arg is given or sample start time >= start_time:
        if not start_time or ts >= start_time:
            if args.clock:
                pub_clock.publish(rospy.Time.from_sec(ts))
            current_mat.publish_ros_object(ts)
        current_mat.sample_count += 1
        cur_wall_time = time.time()

        # Only consider sleeping if no start_time arg is given or sample start
        # time >= start_time:
        if not start_time or ts >= start_time:
            sleep_time = tw + delta_delay - cur_wall_time
            # Check that sleep_time is >= 0.
            # Accounts for the possibility of equal times
            # and/or times that are very close together
            if sleep_time >= 0:
                time.sleep(sleep_time)
                #rospy.sleep(float(sleep_time))
            tw += delta_delay
        ts = next_ts
        ts_list = next_ts_list

def parse_mat_type(filepath, file_types):
    '''
    Return .mat file type (e.g., 'dvl') based on filepath.
    Return None if .mat file is not of type in file_types list.
    Raise an exception if it is not a valid file path.
    '''
    print filepath
    if not os.path.isfile(filepath):
        raise IOError('Not a valid file path.')
    mat_file = os.path.basename(filepath)
    for file_type in file_types:
        # Could use string indices to check, or use regular expressions:
        regex_match = re.search(r'(()*_({0})).mat'.format(file_type), mat_file)
        if regex_match:
            return file_type

def secs_and_nsecs(t):
    return (int(t), int(1e9 * round(t-int(t), 9)))

if __name__ == '__main__':
    files_dict = {'def': DefFile,
                  'gvx': GvxFile,
                  'dvl': DvlFile,
                  'dvz': DvzFile,
                  'hsvp': HsvpFile}
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('-s', '--speed', metavar='FLOAT', default=1.0,
        type=float,
        help=('Simulation speed factor. For example, a value of 2.0 will '
                'replay the data at twice the speed of real time (arg type: '
                'float)'))
    parser.add_argument('-c', '--clock', action='store_true',
        help='Publish simulation time to /clock ROS topic')
    parser.add_argument('path', nargs='+',
        help='.mat file path, at least one required (arg type: string)')
    parser.add_argument('-d', '--start_datetime',
        metavar='yyyy-mm-ddThh:mm:ss.ffffff',
        help=('Datetime at which to begin simulation, in UTC. ISO 8601 format. '
                'Decimal seconds are allowed, but no more than 6 decimal places '
                '(arg type: string)'))
    parser.add_argument('-t', '--start_time_sec', type=float,
        help=('Seconds since epoch, UTC time, at which to begin simulation. '
            'This optional argument will override start_datetime if both are '
            'given (arg type: float)'))
    args = parser.parse_args()
    if args.start_time_sec:
        start_secs = args.start_time_sec
    elif args.start_datetime:
        regex_base = r'\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}'
        if re.match(regex_base+r'\Z', args.start_datetime):
            # Datetime was given without decimal seconds
            start_datetime_obj = datetime.strptime(args.start_datetime, '%Y-%m-%dT%H:%M:%S')
        elif re.match(regex_base+r'\.\d\d?\d?\d?\d?\d?\Z', args.start_datetime):
            # Datetime was given with decimal seconds, 1 to 6 decimal places
            # given. Sequence of question marks in regex limits to 6 decimal
            # places.
            start_datetime_obj = datetime.strptime(args.start_datetime, '%Y-%m-%dT%H:%M:%S.%f')
        elif re.match(regex_base+r'\.\Z', args.start_datetime):
            # Datetime was given with a decimal point at the end, but without decimal seconds
            start_datetime_obj = datetime.strptime(args.start_datetime, '%Y-%m-%dT%H:%M:%S.')
        else:
            # Could instead raise a different exception, e.g., a ValueError
            raise argparse.ArgumentTypeError('Invalid datetime format. '
                                'Please use yyyy-mm-ddThh:mm:ss.ffffff '
                                '(decimal places optional)')
        start_secs = (start_datetime_obj - datetime.utcfromtimestamp(0)).total_seconds()
    else:
        start_secs = None
    # Note: if start_secs is less than the timestamp of the first data sample,
    # the data will replay starting from the first timestamp (as could be
    # expected), and if start_secs is greater than the last of any .mat data
    # file's timestamps, the data will not be replayed.
    # Initialize node:
    rospy.init_node('navest_replay', anonymous=False)
    # Construct a list of instances of specific .mat file classes
    # that inherit the MatFile class:
    mat_list = []
    for f in args.path:
        # Example of a path in args.path:
        # 'sentry428/nav-sci/proc/sentry428_dvl.mat'
        file_type = parse_mat_type(f, files_dict.keys())
        if file_type:
            mat_list.append(files_dict[file_type](f))

    if mat_list:
        run_simulation(mat_list, args.speed, start_secs)
    else:
        print ('Invalid file inputs. Cannot run simulation. '
                'Check the .mat file type (e.g., \'dvl\').')

# TODO note: Not sure if it works or makes sense to ever create multiple
# instances of the same .mat file class

# TODO: Implement robust test for accuracy of sim_speed_factor




