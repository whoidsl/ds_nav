/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_DECKTEST_H
#define DS_DECKTEST_H

#include "ds_nav/ds_nav.h"
#include "ds_sensor_msgs/Ranges3D.h"
#include "ds_sensor_msgs/Dvl.h"

namespace ds_decktest
{
class DsDecktest final : public ds_nav::DsNav
{
public:
  DsDecktest();
  DsDecktest(int argc, char* argv[], const std::string& name);
  ~DsDecktest() override;

  void onRanges3DMsg(const ds_sensor_msgs::Ranges3D::ConstPtr& msg);
  void onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg);

protected:
  void setupSubscriptions() override;
  void setupParameters() override;
  void setupPublishers() override;

private:
  double sim_altitude_;
  double sim_range_;

  ros::Publisher ranges_pub_;
  ros::Publisher dvl_pub_;

  ros::Subscriber ranges_sub_;
  ros::Subscriber dvl_sub_;
};
}

#endif
