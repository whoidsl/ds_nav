/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_decktest/ds_decktest.h"

namespace ds_decktest
{
DsDecktest::DsDecktest() : DsNav()
{
}

DsDecktest::DsDecktest(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
{
}

DsDecktest::~DsDecktest() = default;

void DsDecktest::onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg)
{
  ds_sensor_msgs::Dvl out = *msg;

  out.num_good_beams = 4;
  out.velocity.x = -0.70710678 * 0.2;
  out.velocity.y = 0.70710678 * 0.2;
  out.velocity.z = 0.0;

  dvl_pub_.publish(out);

  ds_sensor_msgs::Ranges3D out2;
  out2.ranges.resize(4);
  for (int i = 0; i < out2.ranges.size(); ++i)
  {
    out2.ranges[i].range.point.x = 0.0;
    out2.ranges[i].range.point.y = 0.0;
    out2.ranges[i].range.point.z = -sim_altitude_;
    out2.ranges[i].range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
    out2.ranges[i].range.header.stamp = msg->header.stamp;
    out2.ranges[i].range.header.frame_id = msg->header.frame_id;
  }
  out2.header.stamp = msg->header.stamp;
  out2.header.frame_id = msg->header.frame_id;

  ranges_pub_.publish(out2);
}

void DsDecktest::onRanges3DMsg(const ds_sensor_msgs::Ranges3D::ConstPtr& msg)
{
  ds_sensor_msgs::Ranges3D out = *msg;

  for (int i = 0; i < out.ranges.size(); ++i)
  {
    out.ranges[i].range.point.x = 0.0;
    out.ranges[i].range.point.y = 0.0;
    out.ranges[i].range.point.z = -sim_altitude_;
    out.ranges[i].range_validity = ds_sensor_msgs::Range3D::RANGE_VALID;
  }

  ranges_pub_.publish(out);
}

void DsDecktest::setupSubscriptions(void)
{
  DsNav::setupSubscriptions();

  auto nh = nodeHandle();
  std::string dvl_input_topic = ros::param::param<std::string>("~dvl_input_topic", "/dvl_input");
  dvl_sub_ = nh.subscribe<ds_sensor_msgs::Dvl>(dvl_input_topic, 1, boost::bind(&DsDecktest::onDvlMsg, this, _1));
  std::string ranges_input_topic = ros::param::param<std::string>("~ranges_input_topic", "/ranges_input");
  // ranges_sub_ = nh.subscribe<ds_sensor_msgs::Ranges3D>(ranges_input_topic, 1, boost::bind(&DsDecktest::onRanges3DMsg,
  // this, _1));
}

void DsDecktest::setupParameters(void)
{
  DsNav::setupParameters();

  sim_altitude_ = ros::param::param<double>("~sim_altitude", 80.0);
  sim_range_ = sim_altitude_ / (sqrt(3.0) / 2.0);
}

void DsDecktest::setupPublishers(void)
{
  DsNav::setupPublishers();

  auto nh = nodeHandle();
  std::string dvl_output_topic = ros::param::param<std::string>("~dvl_output_topic", "/dvl_output");
  dvl_pub_ = nh.advertise<ds_sensor_msgs::Dvl>(dvl_output_topic, 1);
  std::string ranges_output_topic = ros::param::param<std::string>("~ranges_output_topic", "/ranges_output");
  ranges_pub_ = nh.advertise<ds_sensor_msgs::Ranges3D>(ranges_output_topic, 1);
}
}
