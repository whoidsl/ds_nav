/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_EKF_H
#define DS_EKF_H

/*
 *
 *  Discrete-time Extended Kalman Filter generic class.
 *  Equations:
 *
 *  x{k} = f{k-1}(x{k-1},u{k-1},w{k-1})   <--- system equation
 *  y{k} = h{k}(x{k},v{k})                <--- measurement equation
 *  w{k} ~ (0,Q{k})                       <--- process noise
 *  v{k} ~ (0,R{k})                       <--- measurement noise
 *
 *  Author: Stefano Suman <ssuman@whoi.edu>
 *
 *  Revision history:
 *  - June 2018 - ported from dslcommon library
 *                that I wrote in the past which
 *                used VNL. This uses Eigen3
 * */

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace ds_ekf
{
class DsEkf
{
public:
  DsEkf(Eigen::VectorXd x0,  // Initial state
        Eigen::MatrixXd P0,  // Initial covariance matrixs
        int n,               // Number of elements in state vector
        int m,               // Number of measurements
        int nw,              // Number of process noise random variables
        int nv);             // Number of measurement noise random variables
  ~DsEkf();

  int linearize_model(double);     // Linearize nonlinear model function f at x{k-1}+
  int predict(void);               // Find prediction of state estimate and estimation-error covariance
  int linearize_measurement(int);  // Linearize measurement function h at x{k}-
  int update(void);            // Apply measurement update and obtain new state estimate and estimation-error covariance
  int set_u(Eigen::VectorXd);  // Sets the command input u
  int set_y(Eigen::VectorXd);  // Sets the measurement vector y
  Eigen::VectorXd get_x(void);      // Get state estimate x
  Eigen::VectorXd get_u(void);      // Get state estimate input vector
  Eigen::VectorXd get_y(void);      // Get state estimate measurement vector
  Eigen::MatrixXd get_P(void);      // Get covariance matrix P
  Eigen::MatrixXd get_Q(void);      // Get Q
  Eigen::MatrixXd get_K(void);      // Get Kalman gain K
  int init_state(Eigen::VectorXd);  // Inits the state to the specified vector
  int init_P(Eigen::MatrixXd);      // Inits P to the specified matrix

protected:
  // These are pure virtual functions and need to be implemented by classes that inherit from Ekf
  virtual void setF(double) = 0;
  virtual void setL(double) = 0;
  virtual void setH(int) = 0;
  virtual void setM(int) = 0;
  virtual void setQ(int) = 0;
  virtual void setR(int) = 0;
  virtual Eigen::VectorXd setModel(void) = 0;
  virtual Eigen::VectorXd setMeasurement(void) = 0;

  // These are protected internals that can be accessed only by this class
  // and classes that inherit from it.
  // These are all dynamic matrices and vectors
  Eigen::MatrixXd F;   // df(k-1)/dx | x(k-1)+
  Eigen::MatrixXd Ft;  // F transposed
  Eigen::MatrixXd L;   // df(k-1)/dw | x(k-1)+
  Eigen::MatrixXd Lt;  // L transposed
  Eigen::MatrixXd H;   // dh(k)/dx | x(k)-
  Eigen::MatrixXd Ht;  // H transposed
  Eigen::MatrixXd M;   // dh(k)/dv | x(k)-
  Eigen::MatrixXd Mt;  // M transposed
  Eigen::MatrixXd P;   // Estimation error covariance
  Eigen::MatrixXd K;   // Kalman gain matrix
  Eigen::MatrixXd Q;   // Process noise matrix
  Eigen::MatrixXd R;   // Measurement noise matrix
  Eigen::MatrixXd I;   // Identity matrix for estimation-error covariance update
  Eigen::VectorXd x;   // estimated state
  Eigen::VectorXd y;   // measurement vector
  Eigen::VectorXd u;   // command inputs

private:
};
}

#endif
