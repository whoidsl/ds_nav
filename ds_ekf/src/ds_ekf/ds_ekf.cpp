/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_ekf/ds_ekf.h"

namespace ds_ekf
{
DsEkf::~DsEkf() = default;

DsEkf::DsEkf(Eigen::VectorXd x0,  // Initial state
             Eigen::MatrixXd P0,  // Initial covariance matrixs
             int n,               // Number of elements in state vector
             int m,               // Number of measurements
             int nw,              // Number of process noise random variables
             int nv)
  :  // Number of measurement noise random variables
  y(m)
  , F(n, n)
  , Ft(n, n)
  , Q(n, n)
  , H(m, n)
  , Ht(n, m)
  , R(m, m)
  , K(n, m)
  , I(Eigen::MatrixXd::Identity(n, n))
  , L(nw, n)
  , Lt(n, nw)
  , M(nv, m)
  , Mt(m, nv)
  , x(x0)
  , P(P0)
{
}

// Find the new F, L matrices
int DsEkf::linearize_model(double dt)
{
  setF(dt);
  setL(dt);
}

// Find prediction of state estimate and estimation-error covariance
int DsEkf::predict(void)
{
  Ft = F.transpose();
  Lt = L.transpose();

  P = F * P * Ft + L * Q * Lt;
  x = setModel();
}

// Find the new H, M matrices
int DsEkf::linearize_measurement(int type)
{
  setH(type);
  setM(type);
}

// Apply measurement update and obtain new state estimate and estimation-error covariance
int DsEkf::update(void)
{
  Ht = H.transpose();
  Mt = M.transpose();

  K = P * Ht * (H * P * Ht + M * R * Mt).inverse();
  x = x + K * (y - setMeasurement());
  P = (I - K * H) * P;
}

// Set command input u
int DsEkf::set_u(Eigen::VectorXd u_in)
{
  u = u_in;
}

int DsEkf::set_y(Eigen::VectorXd y_in)
{
  y = y_in;
}

int DsEkf::init_state(Eigen::VectorXd x_in)
{
  x = x_in;
}

int DsEkf::init_P(Eigen::MatrixXd P_in)
{
  P = P_in;
}

// Get state estimate x
Eigen::VectorXd DsEkf::get_x(void)
{
  return x;
}

Eigen::VectorXd DsEkf::get_y(void)
{
  return y;
}

Eigen::VectorXd DsEkf::get_u(void)
{
  return u;
}

Eigen::MatrixXd DsEkf::get_P(void)
{
  return P;
}

Eigen::MatrixXd DsEkf::get_Q(void)
{
  return Q;
}

Eigen::MatrixXd DsEkf::get_K(void)
{
  return K;
}
}
