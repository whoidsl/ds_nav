cmake_minimum_required(VERSION 2.8.3)
project(ds_ekf)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(catkin REQUIRED COMPONENTS
  cmake_modules
)

find_package(ClangFormat)
if(CLANG_FORMAT_FOUND)
    add_custom_target(clang-format-ds-ekf
            COMMENT
            "Run clang-format on all project C++ sources"
            WORKING_DIRECTORY
            ${PROJECT_SOURCE_DIR}
            COMMAND
            find src
            include/ds_ekf
            -iname '*.h' -o -iname '*.cpp'
            | xargs ${CLANG_FORMAT_EXECUTABLE} -i
            )
endif(CLANG_FORMAT_FOUND)

find_package(Eigen3 REQUIRED)

catkin_package(
  INCLUDE_DIRS include
  LIBRARIES ds_ekf
  DEPENDS EIGEN3
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
  include/${PROJECT_NAME}/ds_ekf.h
  src/${PROJECT_NAME}/ds_ekf.cpp
)

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)
