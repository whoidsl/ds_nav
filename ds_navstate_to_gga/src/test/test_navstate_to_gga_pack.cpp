//
// Created by jvaccaro on 5/30/19.
//
/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "ds_navstate_to_gga/ds_navstate_to_gga.h"
#include "ds_util/int_to_hex.h"
#include <gtest/gtest.h>
#include "ds_nmea_parsers/Gga.h"
#include "ds_nmea_parsers/util.h"

TEST(NAVSTATE_TO_GGA, valid) {
  
  
  const auto test_pairs =
      std::list<std::pair<double,const std::string>>{
          { 0.0, "$GPGGA,000000.000,0000.000000,N,00000.000000,E,2,08,1.0,-0,M,0.000,M,1012*48"},
          { 5.0, "$GPGGA,000000.000,0500.000000,N,00500.000000,E,2,08,1.0,-5,M,0.000,M,1012*48"},
          { 85.7, "$GPGGA,000000.000,8542.000000,N,08542.000000,E,2,08,1.0,-85.7,M,0.000,M,1012*48"},
          { -23.99, "$GPGGA,000000.000,2359.400000,S,02359.400000,W,2,08,1.0,23.99,M,0.000,M,1012*47"}
      };
  for (const auto& test_pair : test_pairs)
  {
    ds_nav_msgs::NavState msg;
    msg.lat = test_pair.first;
    msg.lon = test_pair.first;
    msg.down = test_pair.first;
    std::string expected_str = test_pair.second;
    ds_nmea_msgs::Gga expected;
    ASSERT_TRUE(ds_nmea_msgs::from_string(expected, expected_str));
    std::string actual_str = ds_nav::NavstateToGga::pack_message(msg);

    ds_nmea_msgs::Gga actual;
    ASSERT_TRUE(ds_nmea_msgs::from_string(actual, actual_str));
    ASSERT_EQ(expected.latitude, actual.latitude);
    ASSERT_EQ(expected.longitude, actual.longitude);
    ASSERT_EQ(expected.antenna_alt, actual.antenna_alt);
    ASSERT_EQ(expected.checksum, actual.checksum);
    ASSERT_EQ(ds_nmea_msgs::calculate_checksum(expected_str),
              ds_nmea_msgs::calculate_checksum(actual_str));
    ASSERT_EQ(expected.timestamp, actual.timestamp);
  }
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}