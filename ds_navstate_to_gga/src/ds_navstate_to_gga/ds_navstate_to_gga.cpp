//
// Created by jvaccaro on 5/30/19.
//
/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_navstate_to_gga/ds_navstate_to_gga.h"
#include "ds_nmea_parsers/util.h"
#include "ds_util/int_to_hex.h"

namespace ds_nav {

NavstateToGga::NavstateToGga()
    : DsProcess()
{
}

NavstateToGga::NavstateToGga(int argc, char* argv[], const std::string& name)
    : DsProcess(argc, argv, name)
{
}

NavstateToGga::~NavstateToGga(){
  m_spitter.stop();
}

std::string
NavstateToGga::pack_message(ds_nav_msgs::NavState msg)
{
  std::stringstream ss;
  ss << "$GPGGA" << ",";
  ss << ds_nmea_msgs::to_nmea_utc_str(msg.header.stamp) << ","; // 1. Time (UTC)
  ss << ds_nmea_msgs::to_nmea_lat_string(msg.lat); // 2. Lat  3. N/S
  ss << ds_nmea_msgs::to_nmea_lon_string(msg.lon); // 4. Lon  5. E/W
  ss << "2" << ","; // 6. GPS Quality indicator (set to GPS, not DGPS)
  ss << "08" << ","; // 7. Number of satellites in view, pretend it's sufficient
  ss << "1.0" << ","; // 8. Horizontal Dilution of precision
  ss << -1.0*msg.down << ","; // 9. Antenna altitude above/below mean-sea-level
  ss << "M" << ","; // 10. Units of antenna altitude, meters
  ss << "0.000" << ","; // 11) Geoidal separation, the difference between the WGS-84 earth ellipsoid and mean-sea-level (geoid), "-" means mean-sea-level below ellipsoid
  ss << "M" << ","; // 12) Units of geoidal separation, meters
  ss << "1012"; // 13) Age of differential GPS data, time in seconds since last SC104 type 1 or 9 update, null field when DGPS is not used
  ss << "*"; // 14) Differential reference station ID, 0000-1023
  // Now calculate the checksum
  uint16_t checksum = ds_nmea_msgs::calculate_checksum(ss.str());

  // Append the checksum
  std::string checksum_str = ds_util::int_to_hex<uint16_t>(checksum);
  ss << checksum_str << "\r\n"; // 15) Checksum
  return ss.str();
}

void
NavstateToGga::setupSubscriptions()
{
  ds_base::DsProcess::setupSubscriptions();
  std::string navstate_topic = ros::param::param<std::string>("~navstate_topic", "navstate");
  m_nav_sub = nodeHandle().subscribe<ds_nav_msgs::NavState>(
      navstate_topic, 1, boost::bind(&NavstateToGga::_on_nav_msg, this, _1));
}

void
NavstateToGga::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  m_gga_conn = addConnection("gga", boost::bind(&NavstateToGga::_on_gga_msg, this, _1));
  m_is_spitter = ros::param::param<bool>("~is_spitter", false);
  m_spitter = nodeHandle().createTimer(ros::Duration(1), &NavstateToGga::_on_spitter_timer, this);
  m_spitter.start();
}

void
NavstateToGga::_on_nav_msg(const ds_nav_msgs::NavState::ConstPtr& msg)
{
  std::string out_msg = pack_message(*msg);
  m_gga_conn->send(out_msg);
}

void
NavstateToGga::_on_gga_msg(const ds_core_msgs::RawData& msg)
{
}

void
NavstateToGga::_on_spitter_timer(const ros::TimerEvent&){
  if (m_is_spitter){
    ds_nav_msgs::NavState msg;
    msg.ds_header.io_time = ros::Time::now();
    msg.header.stamp = msg.ds_header.io_time;
    msg.lat = -15;
    msg.lon = 85;
    msg.down = 20;
    std::string out_msg = pack_message(msg);
    m_gga_conn->send(out_msg);
  }
}
} //namespace