cmake_minimum_required(VERSION 2.8.3)
project(ds_deadreck)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

find_package(catkin REQUIRED COMPONENTS
  ds_nav
  cmake_modules
  ds_sensor_msgs
  ds_core_msgs
  ds_nav_msgs
  tf2_ros
  tf2_eigen
  )

find_package(ClangFormat)
if(CLANG_FORMAT_FOUND)
    add_custom_target(clang-format-ds-deadreck
            COMMENT
            "Run clang-format on all project C++ sources"
            WORKING_DIRECTORY
            ${PROJECT_SOURCE_DIR}
            COMMAND
            find src
            include/ds_deadreck
            -iname '*.h' -o -iname '*.cpp'
            | xargs ${CLANG_FORMAT_EXECUTABLE} -i
            )
endif(CLANG_FORMAT_FOUND)

find_package(Eigen3 REQUIRED)

###################################
## catkin specific configuration ##
###################################

catkin_package(
  INCLUDE_DIRS include
  CATKIN_DEPENDS ds_nav ds_sensor_msgs ds_nav_msgs tf2_ros
  DEPENDS EIGEN3
)

###########
## Build ##
###########

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
  ${EIGEN3_INCLUDE_DIR}
  )

add_library(${PROJECT_NAME}
  include/ds_deadreck/ds_deadreck.h
  src/ds_deadreck.cpp
)

add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

add_executable(ds_deadreck_node
  include/ds_deadreck/ds_deadreck.h
  src/main.cpp
  src/ds_deadreck.cpp
)

target_link_libraries(ds_deadreck_node
  ${catkin_LIBRARIES}
)

#############
## Install ##
#############

install(TARGETS ds_deadreck_node
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY
  launch
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
)

#############
## Testing ##
#############
if(CATKIN_ENABLE_TESTING)
    find_package(rostest REQUIRED)
    add_rostest_gtest(test_ds_deadreck
            test/test_ds_deadreck.test
            src/test/test_ds_deadreck.cpp
            )
    target_link_libraries(test_ds_deadreck ${PROJECT_NAME} ${catkin_LIBRARIES})
endif(CATKIN_ENABLE_TESTING)
