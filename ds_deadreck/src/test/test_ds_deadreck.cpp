/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_deadreck/ds_deadreck.h"
#include <gtest/gtest.h>

// This allows us to call protected functions
class DsDeadreckFixture : public ds_deadreck::DsDeadreck, public testing::Test
{
protected:
  void SetUp() override
  {
    geometry_msgs::TransformStamped msg;

    // Insert transform in the tf buffer before setup (setup looks up the buffer)
    // DVL transform with "standard" positioning of the DVL
    msg.header.stamp = ros::Time(0);
    msg.header.frame_id = "base_link";
    msg.child_frame_id = "rdi300_link";
    msg.transform.translation = tf2::toMsg(tf2::Vector3(-0.462, 0.0, -0.829));
    msg.transform.rotation = tf2::toMsg(tf2::Quaternion(0.0, 0.0, 0.923879532511, -0.382683432366));
    setTransform(msg, "manual");

    // Phins transform with "standard" positioning of the Phins
    msg.header.stamp = ros::Time(0);
    msg.header.frame_id = "base_link";
    msg.child_frame_id = "phins_link";
    msg.transform.translation = tf2::toMsg(tf2::Vector3(-1.112, 0.005, 0.241));
    msg.transform.rotation = tf2::toMsg(tf2::Quaternion(0.0, 0.0, 0.0, 1.0));
    setTransform(msg, "manual");

    // paro transform with "standard" positioning of the paro
    msg.header.stamp = ros::Time(0);
    msg.header.frame_id = "base_link";
    msg.child_frame_id = "paro_link";
    msg.transform.translation = tf2::toMsg(tf2::Vector3(-1.895, 0.177, -0.249));
    msg.transform.rotation = tf2::toMsg(tf2::Quaternion(0.0, 0.0, 0.0, 1.0));
    setTransform(msg, "manual");

    setup();
  }

  // Generates a quaternion from Phins frame to world E-N-U frame
  tf2::Quaternion quaternionHPR(double heading, double pitch, double roll)
  {
    tf2::Quaternion q_heading(tf2::Vector3(0, 0, 1), dtor(-heading + 90));
    tf2::Quaternion q_pitch(tf2::Vector3(0, 1, 0), dtor(pitch));
    tf2::Quaternion q_roll(tf2::Vector3(1, 0, 0), dtor(roll));
    tf2::Quaternion q;

    q = q_heading;  // Start with roll
    q.normalize();
    q *= q_pitch;  // Multiply by pitch on the right
    q.normalize();
    q *= q_roll;  // Multiply the result by heading on the right
    q.normalize();

    q = (q_heading * q_pitch * q_roll).normalize();
    return q;
  }
};

TEST_F(DsDeadreckFixture, checkQuaternion)
{
  tf2::Quaternion world;

  /// HEADING
  // Vehicle aligned with E-N-U
  world = quaternionHPR(90, 0, 0);
  ASSERT_NEAR(world.x(), 0.0, 0.000001);
  ASSERT_NEAR(world.y(), 0.0, 0.000001);
  ASSERT_NEAR(world.z(), 0.0, 0.000001);
  ASSERT_NEAR(world.w(), 1.0, 0.000001);

  // Rotate 90 degrees
  world = quaternionHPR(180.0, 0, 0);
  ASSERT_NEAR(world.x(), 0.0, 0.000001);
  ASSERT_NEAR(world.y(), 0.0, 0.000001);
  ASSERT_NEAR(world.z(), -0.707107, 0.000001);
  ASSERT_NEAR(world.w(), 0.707107, 0.000001);

  // Rotate 180 degrees
  world = quaternionHPR(270.0, 0, 0);
  ASSERT_NEAR(world.x(), 0.0, 0.000001);
  ASSERT_NEAR(world.y(), 0.0, 0.000001);
  ASSERT_NEAR(world.z(), -1.0, 0.000001);
  ASSERT_NEAR(world.w(), 0.0, 0.000001);

  // Rotate 270 degrees
  world = quaternionHPR(360.0, 0, 0);
  ASSERT_NEAR(world.x(), 0.0, 0.000001);
  ASSERT_NEAR(world.y(), 0.0, 0.000001);
  ASSERT_NEAR(world.z(), -0.707107, 0.000001);
  ASSERT_NEAR(world.w(), -0.707107, 0.000001);

  /// PITCH
  world = quaternionHPR(90, 90, 0);
  ASSERT_NEAR(world.x(), 0.0, 0.000001);
  ASSERT_NEAR(world.y(), 0.707107, 0.000001);
  ASSERT_NEAR(world.z(), 0.0, 0.000001);
  ASSERT_NEAR(world.w(), 0.707107, 0.000001);

  /// ROLL
  world = quaternionHPR(90, 0, 90);
  ASSERT_NEAR(world.x(), 0.707107, 0.000001);
  ASSERT_NEAR(world.y(), 0.0, 0.000001);
  ASSERT_NEAR(world.z(), 0.0, 0.000001);
  ASSERT_NEAR(world.w(), 0.707107, 0.000001);

  /// Combined
  world = quaternionHPR(45, 90, 0);
  ASSERT_NEAR(world.x(), -0.270598, 0.0001);
  ASSERT_NEAR(world.y(), 0.653281, 0.0001);
  ASSERT_NEAR(world.z(), 0.270598, 0.0001);
  ASSERT_NEAR(world.w(), 0.653281, 0.0001);
}

TEST_F(DsDeadreckFixture, checkVelocityRotationToVehicleFrame)
{
  Eigen::Vector3d vel_in;
  Eigen::Vector3d vel_out;

  // Fwd motion
  vel_in[0] = -0.707107;
  vel_in[1] = 0.707107;
  vel_in[2] = 0.0;
  vel_out = rotateVel(vel_in);
  ASSERT_NEAR(vel_out[0], 1.0, 0.00001);
  ASSERT_NEAR(vel_out[1], 0.0, 0.00001);
  ASSERT_NEAR(vel_out[2], 0.0, 0.00001);

  // Stbd motion
  vel_in[0] = 0.707107;
  vel_in[1] = 0.707107;
  vel_in[2] = 0.0;
  vel_out = rotateVel(vel_in);
  ASSERT_NEAR(vel_out[0], 0.0, 0.00001);
  ASSERT_NEAR(vel_out[1], -1.0, 0.00001);
  ASSERT_NEAR(vel_out[2], 0.0, 0.00001);
}

TEST_F(DsDeadreckFixture, checkIntegration)
{
  // Input velocities in vehicle frame FWD-PORT-UP
  Eigen::Vector3d vel;
  Eigen::Vector3d world_vel;
  double dt;

  // Push a heading of 45 degrees
  boost::shared_ptr<ds_sensor_msgs::Ins> ins(new ds_sensor_msgs::Ins());
  ins->orientation = tf2::toMsg(quaternionHPR(45, 0, 0));
  onInsMsg(ins);
  onInsMsg(ins);

  // Move forward
  vel[0] = 1.0;
  vel[1] = 0.0;
  vel[2] = 0.0;

  world_vel = integrate(vel, 1.0);
  auto outState = getState();
  ASSERT_NEAR(world_vel[0], 0.707107, 0.0001);
  ASSERT_NEAR(world_vel[1], 0.707107, 0.0001);
  ASSERT_NEAR(world_vel[2], 0.0, 0.0001);
  ASSERT_NEAR(outState.easting, 0.707107, 0.0001);
  ASSERT_NEAR(outState.northing, 0.707107, 0.0001);
  ASSERT_NEAR(outState.down, 0.0, 0.000001);

  ins->orientation = tf2::toMsg(quaternionHPR(45, 90, 0));
  onInsMsg(ins);
  onInsMsg(ins);

  // Move forward
  vel[0] = 0.0;
  vel[1] = 0.0;
  vel[2] = 1.0;

  world_vel = integrate(vel, 1.0);
  outState = getState();
  ASSERT_NEAR(world_vel[0], 0.707107, 0.0001);
  ASSERT_NEAR(world_vel[1], 0.707107, 0.0001);
  ASSERT_NEAR(world_vel[2], 0.0, 0.0001);
  ASSERT_NEAR(outState.easting, sqrt(2.0), 0.0001);
  ASSERT_NEAR(outState.northing, sqrt(2.0), 0.0001);
  ASSERT_NEAR(outState.down, 0.0, 0.000001);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::init(argc, argv, "ds_deadreck");
  testing::InitGoogleTest(&argc, argv);
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();
  return ret;
};
