/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_deadreck/ds_deadreck.h"

namespace ds_deadreck
{
DsDeadreck::~DsDeadreck() = default;

DsDeadreck::DsDeadreck() : DsNav()
{
  initialize();
}

DsDeadreck::DsDeadreck(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
{
  initialize();
}

void DsDeadreck::initialize(void)
{
  ROS_INFO_STREAM("Creating deadreck node");
  dvl_scaled_[0] = 0.0;
  dvl_scaled_[1] = 0.0;
  dvl_scaled_[2] = 0.0;
  modelState_.surge_u = 0.0;
  ins_.push_back(ds_sensor_msgs::Ins());
  gyro_.push_back(ds_sensor_msgs::Gyro());
  soundSpeed_.push_back(ds_sensor_msgs::SoundSpeed());
  depthPressure_.push_back(ds_sensor_msgs::DepthPressure());

  // initialize to known values.  Otherwise, we
  // can create NaNs if we try to e.g., integrate before we get a valid
  // gyro sample
  dvl_scaled_ = Eigen::Vector3d::Zero();
  dvl_scaled_tfd_ = Eigen::Vector3d::Zero();
  dvl_scaled_tfd_world_ = Eigen::Vector3d::Zero();
  gyro_a_ = Eigen::Affine3d::Identity();
  gyro_tfd_ = Eigen::Affine3d::Identity();
  offset_ = Eigen::Vector3d::Zero();

  dvlNavState_.easting = 0.0;
  dvlNavState_.northing = 0.0;
  dvlNavState_.down = 0.0;
  vehicleNavState_.easting = 0.0;
  vehicleNavState_.northing = 0.0;
  vehicleNavState_.down = 0.0;

  time_last_good_bt_ = ros::Time(0);
  last_model_time_ = ros::Time(0);

  auto myUuid = uuid();
  std::copy(std::begin(myUuid), std::end(myUuid), std::begin(dvlNavState_.ds_header.source_uuid));
}

void DsDeadreck::onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg)
{
  // This guards against issues with boost::gregorian or boost::posix time
  // in the dvl driver: if the dvl driver has an exception in determining the dvl time,
  // then the dvl time will be 0
  if ((msg->dvl_time > 0) && (std::isfinite(msg->velocity.x)) && (std::isfinite(msg->velocity.y)) &&
      (std::isfinite(msg->velocity.z)))
  {
    // Store last received message
    dvl_.push_back(*msg);

    if (dvl_.full())
    {
      if ((dvl_.getLast().num_good_beams > 2) &&
	  (fabs(dvl_.getLast().dvl_time - dvl_.getPrevious().dvl_time) < 10.0)) // 2019-03-13 SS - hack for dvl byte swapping on date
      {
        dr_.active_velocity_source = ds_nav_msgs::DeadReck::SOURCE_DVL;
        time_last_good_bt_ = ros::Time::now();
        dr_.used_ss = default_ss_;
        if (soundSpeed_.full())
          dr_.used_ss = soundSpeed_.getLast().speed;
        // Guard agains nan in the division if both arguments are exactly 0
        if (dr_.used_ss == 0.0)
          dr_.used_ss = default_ss_;
        dr_.dvl_ss = dvl_.getLast().speed_sound;
        // Guard against bad DVL velocity
        if (dr_.dvl_ss > dvlss_max_ || dr_.dvl_ss < dvlss_min_)
        {
          dr_.dvl_ss = dvlss_assumed_;
          ROS_ERROR_STREAM("DVL reported velocity " << dr_.dvl_ss << " outside valid range.  Setting to assumed "
                                                                     "velocity");
        }
        dr_.scale_factor = dr_.used_ss / dr_.dvl_ss;

        dr_.dvl_speed = dvl_.getLast().velocity;
        dvl_scaled_[0] = dvl_.getLast().velocity.x * dr_.scale_factor;
        dvl_scaled_[1] = dvl_.getLast().velocity.y * dr_.scale_factor;
        dvl_scaled_[2] = dvl_.getLast().velocity.z * dr_.scale_factor;
        dr_.dvl_scaled.x = dvl_scaled_[0];
        dr_.dvl_scaled.y = dvl_scaled_[1];
        dr_.dvl_scaled.z = dvl_scaled_[2];

        dr_.dt = dvl_.getLast().dvl_time - dvl_.getPrevious().dvl_time;

        dvl_scaled_tfd_ = rotateVel(dvl_scaled_);
        dr_.dvl_scaled_tfd.x = dvl_scaled_tfd_[0];
        dr_.dvl_scaled_tfd.y = dvl_scaled_tfd_[1];
        dr_.dvl_scaled_tfd.z = dvl_scaled_tfd_[2];

        integrate(dvl_scaled_tfd_, dr_.dt);
        publish(msg->header.stamp);
      }
      else if ((dvl_.getLast().num_good_beams <= 2) &&
               ((dvl_.getLastTime() - time_last_good_bt_).toSec() < dvl_timeout_) &&
	       (fabs(dvl_.getLast().dvl_time - dvl_.getPrevious().dvl_time) < 10.0))  // 2019-03-13 SS - hack for dvl byte swapping on date
      {
        // Extrapolate using last known dvl_scaled velocities, until the model velocities kick in after dvl_timeout
        // seconds
        dr_.active_velocity_source = ds_nav_msgs::DeadReck::SOURCE_DVL_HOLD;
        dr_.dt = dvl_.getLast().dvl_time - dvl_.getPrevious().dvl_time;

        dvl_scaled_tfd_ = rotateVel(dvl_scaled_);
        dr_.dvl_scaled_tfd.x = dvl_scaled_tfd_[0];
        dr_.dvl_scaled_tfd.y = dvl_scaled_tfd_[1];
        dr_.dvl_scaled_tfd.z = dvl_scaled_tfd_[2];

        integrate(dvl_scaled_tfd_, dr_.dt);
        publish(msg->header.stamp);
      }
      else
      {
        ROS_WARN_STREAM("Lost good bottom lock for more than " << dvl_timeout_ << " seconds, using model velocities");
      }
    }
  }
  else
  {
    ROS_WARN_STREAM("dvl_time is 0, driver may be incorrectly parsing time");
  }
}

Eigen::Vector3d DsDeadreck::rotateVel(Eigen::Vector3d in)
{
  // Transform Dvl velocities to vehicle frame
  Eigen::Vector3d out = dvl_.rotation() * in;

  return out;
}

Eigen::Vector3d DsDeadreck::integrate(Eigen::Vector3d vel, double dt)
{
  ros::Time now = ros::Time::now();
  dvlNavState_.header.stamp = now;
  dvlNavState_.ds_header.io_time = now;

  dvlNavState_.surge_u = dvl_scaled_tfd_[0];
  dvlNavState_.sway_v = -dvl_scaled_tfd_[1];
  dvlNavState_.heave_w = -dvl_scaled_tfd_[2];
  dvl_scaled_tfd_world_ = gyro_tfd_.rotation() * vel;
  dr_.dvl_scaled_tfd_world.x = dvl_scaled_tfd_world_[0];
  dr_.dvl_scaled_tfd_world.y = dvl_scaled_tfd_world_[1];
  dr_.dvl_scaled_tfd_world.z = dvl_scaled_tfd_world_[2];

  // Integrate (dvl_scaled_tfd_world_ is in N-W-U frame, need to go to E-N-U/N-E-D)
  dvlNavState_.easting += dvl_scaled_tfd_world_[0] * dt;
  dvlNavState_.northing += dvl_scaled_tfd_world_[1] * dt;
  ROS_INFO_STREAM("Position:            " << dvlNavState_.easting << " " << dvlNavState_.northing << " "
                                          << dvlNavState_.down);

  // double distance_2d = sqrt(pow((dvl_scaled_tfd_world_[1] * dt), 2) + pow((dvl_scaled_tfd_world_[0] * dt), 2));
  // Also do geodesic integration
  // std::tie(dvlNavState_.lat, dvlNavState_.lon) = directGeodesic(dvlNavState_.lat, dvlNavState_.lon,
  // dvlNavState_.down, rtod(dvlNavState_.heading), distance_2d);

  // The DVL integrator tracks DVL position, NOT vehicle position.  We therefore
  // apply the offset from DVL -> vehicle every iteration
  vehicleNavState_ = dvlNavState_;

  // Compute the offset translation from dvl frame to target frame
  offset_ = gyro_tfd_ * -dvl_.translation();
  double offset_2d = sqrt(offset_[0] * offset_[0] + offset_[1] * offset_[1]);
  double offset_hdg = atan2(offset_[0], offset_[1]);
  ROS_INFO_STREAM("Offset: " << offset_.transpose());
  ROS_INFO_STREAM("Offset: " << offset_2d << " @" << 180.0 / M_PI * offset_hdg
                             << " veh: " << rtod(dvlNavState_.heading));
  vehicleNavState_.easting += offset_[0];
  vehicleNavState_.northing += offset_[1];
  // std::tie(vehicleNavState_.lat, vehicleNavState_.lon) = directGeodesic(dvlNavState_.lat, dvlNavState_.lon,
  // dvlNavState_.down, rtod(offset_hdg), offset_2d);
  std::tie(vehicleNavState_.lat, vehicleNavState_.lon) = xy2latlon(vehicleNavState_.easting, vehicleNavState_.northing);

  return dvl_scaled_tfd_world_;
}

void DsDeadreck::publish(ros::Time in)
{
  Eigen::Quaterniond q(gyro_tfd_.rotation());
  geometry_msgs::Quaternion qg = tf2::toMsg(q);

  vehicleNavState_.header.stamp = in;
  dvlNavState_.header.stamp = in;
  dr_.header.stamp = in;

  pub_["NavState"].publish(vehicleNavState_);
  pub_["DeadReck"].publish(dr_);
  sendPose(pub_["PoseStamped"], odometry_frame_, dvlNavState_.easting, dvlNavState_.northing,
           -dvlNavState_.down - offset_[2], qg);
}

ds_nav_msgs::NavState& DsDeadreck::getState(void)
{
  return dvlNavState_;
}

void DsDeadreck::onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received Gyro message");

  gyro_.push_back(*msg);

  updateRph(msg->roll, msg->pitch, msg->heading, msg->orientation);
}

void DsDeadreck::onInsMsg(const ds_sensor_msgs::Ins::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received Ins message");

  ins_.push_back(*msg);

  updateRph(msg->roll, msg->pitch, msg->heading, msg->orientation);
}

void DsDeadreck::updateRph(double roll, double pitch, double heading, geometry_msgs::Quaternion q)
{
  dvlNavState_.roll = roll;
  dvlNavState_.pitch = pitch;
  dvlNavState_.heading = heading;

  Eigen::Quaterniond out;
  tf2::fromMsg(q, out);

  // Assign quaternion to the affine 3D trasnformation
  gyro_a_ = out;

  gyro_tfd_ = gyro_.rotation() * gyro_a_.rotation();
  Eigen::Quaterniond qe(gyro_tfd_.rotation());
  geometry_msgs::Quaternion qg = tf2::toMsg(qe);
  vehicleNavState_.orientation = qg;
  dvlNavState_.orientation = qg;
}

void DsDeadreck::onDepthPressureMsg(const ds_sensor_msgs::DepthPressure::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received Depth message");

  depthPressure_.push_back(*msg);

  dvlNavState_.down = msg->depth;
}

void DsDeadreck::onSoundSpeedMsg(const ds_sensor_msgs::SoundSpeed::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received SoundSpeed message, sound_speed: " << msg->speed);

  soundSpeed_.push_back(*msg);
}

void DsDeadreck::onModelStateMsg(const ds_nav_msgs::ModelState::ConstPtr& msg)
{
  ROS_DEBUG_STREAM("Received ModelState message");

  modelState_ = *msg;

  ros::Time now = ros::Time::now();

  if (last_model_time_ == ros::Time(0))
    last_model_time_ = now;

  if (((now - time_last_good_bt_).toSec() > dvl_timeout_) && (std::isfinite(modelState_.surge_u)))
  {
    dr_.active_velocity_source = ds_nav_msgs::DeadReck::SOURCE_MODEL;

    // Use model velocities: they are already in the vehicle frame so they do not need to be rotated by the Dvl
    // alignemnt;
    dvl_scaled_tfd_[0] = modelState_.surge_u;
    dvl_scaled_tfd_[1] = 0.0;
    dvl_scaled_tfd_[2] = 0.0;
    dr_.dvl_scaled_tfd.x = dvl_scaled_tfd_[0];
    dr_.dvl_scaled_tfd.y = dvl_scaled_tfd_[1];
    dr_.dvl_scaled_tfd.z = dvl_scaled_tfd_[2];
    dr_.dt = (now - last_model_time_).toSec();
    integrate(dvl_scaled_tfd_, dr_.dt);
    publish(msg->header.stamp);
  }
  last_model_time_ = now;
}

void DsDeadreck::onSacTimer(const ros::TimerEvent& event)
{
  if (dvl_.full())
    ROS_INFO_STREAM("Dvl age: " << dvl_.age());
}

void DsDeadreck::resetDvlPosition(double lat, double lon)
{
  double x, y;
  ROS_INFO_STREAM("Resetting dvl position, lat: " << lat << " lon: " << lon);
  std::tie(x, y) = latlon2xy(lat, lon);
  ROS_INFO_STREAM("\\---> DVL Reset x/y: " << x << " " << y);
  dvlNavState_.lat = lat;
  dvlNavState_.lon = lon;
  dvlNavState_.easting = x;
  dvlNavState_.northing = y;
}

void DsDeadreck::resetDvlPositionXY(double easting, double northing)
{
  double lat, lon;
  ROS_ERROR_STREAM("Resetting dvl position, easting: " << easting << " northing: " << northing);
  dvlNavState_.easting = easting;
  dvlNavState_.northing = northing;
  std::tie(lat, lon) = xy2latlon(easting, northing);
  ROS_INFO_STREAM("\\---> DVL Reset lat/lon: " << lat << " " << lon);
  dvlNavState_.lat = lat;
  dvlNavState_.lon = lon;
}

void DsDeadreck::updateOrigin(double lat, double lon)
{
  setOrigin(lat, lon);
}

void DsDeadreck::setDvlTimeout(double timeout)
{
  dvl_timeout_ = timeout;
}

bool DsDeadreck::_reset_dvl_cmd(const ds_nav_msgs::ResetDvl::Request& req, const ds_nav_msgs::ResetDvl::Response& resp)
{
  resetDvlPositionXY(req.reset_easting, req.reset_northing);

  return true;
}

void DsDeadreck::setupParameters()
{
  DsNav::setupParameters();

  double origin_lat = ros::param::param<double>(ros::this_node::getNamespace() + "/origin_lat", 0);
  double origin_lon = ros::param::param<double>(ros::this_node::getNamespace() + "/origin_lon", 0);
  ROS_INFO_STREAM("Origin lat: " << origin_lat << " Origin lon: " << origin_lon);
  if (std::isfinite(origin_lat) && std::isfinite(origin_lon))
  {
    updateOrigin(origin_lat, origin_lon);
  }
  else
  {
    ROS_ERROR_STREAM("ORIGIN is NaN!  NOT SETTING ORIGIN CORRECTLY!");
    origin_lat = 0;
    origin_lon = 0;
  }

  double start_lat = ros::param::param<double>("~start_lat", origin_lat);
  double start_lon = ros::param::param<double>("~start_lon", origin_lon);
  if (!std::isfinite(start_lat) || !std::isfinite(start_lon))
  {
    start_lat = origin_lat;
    start_lon = origin_lon;
    ROS_ERROR_STREAM("Deadreck start lat or lon is not-finite! Resetting both to origin");
  }
  ROS_INFO_STREAM("Start lat: " << start_lat << " Start lon: " << start_lon);
  resetDvlPosition(start_lat, start_lon);

  default_ss_ = ros::param::param<double>("~default_ss", 1500);

  double timeout = ros::param::param<double>("~dvl_timeout", 5);
  setDvlTimeout(timeout);

  // Output frames
  odometry_frame_ = ros::param::param<std::string>("~odometry_frame", "odom");
  map_frame_ = ros::param::param<std::string>("~map_frame", "map");

  // Input frames for which we need to get transforms from tf2
  dvl_frame_ = ros::param::param<std::string>("~dvl_frame", "rdi300_link");
  gyro_ins_frame_ = ros::param::param<std::string>("~gyro_ins_frame", "phins_link");
  depth_frame_ = ros::param::param<std::string>("~depth_frame", "paro_link");

  // Frame in which the nav solution will be computed
  target_frame_ = ros::param::param<std::string>("~target_frame", "base_link");

  // Load parameters for the DVL sound speed checker (yes, the fixed sound speed reported
  // by the DVL driver.  The one that should always be 1500m/s)
  dvlss_min_ = ros::param::param<double>("~dvlss_min", 1450);
  dvlss_max_ = ros::param::param<double>("~dvlss_max", 1575);
  dvlss_assumed_ = ros::param::param<double>("~dvlss_assumed", 1500);
}

void DsDeadreck::setupSubscriptions()
{
  DsNav::setupSubscriptions();

  std::string dvl_ns = ros::param::param<std::string>("~dvl_ns", "0");
  sub_["Dvl"] = nodeHandle().subscribe<ds_sensor_msgs::Dvl>(dvl_ns, 1, boost::bind(&DsDeadreck::onDvlMsg, this, _1));
  std::string gyro_ns = ros::param::param<std::string>("~gyro_ns", "0");
  sub_["Gyro"] =
      nodeHandle().subscribe<ds_sensor_msgs::Gyro>(gyro_ns, 1, boost::bind(&DsDeadreck::onGyroMsg, this, _1));
  std::string ins_ns = ros::param::param<std::string>("~ins_ns", "0");
  sub_["Ins"] = nodeHandle().subscribe<ds_sensor_msgs::Ins>(ins_ns, 1, boost::bind(&DsDeadreck::onInsMsg, this, _1));
  std::string depth_ns = ros::param::param<std::string>("~depth_ns", "0");
  sub_["DepthPressure"] = nodeHandle().subscribe<ds_sensor_msgs::DepthPressure>(
      depth_ns, 1, boost::bind(&DsDeadreck::onDepthPressureMsg, this, _1));
  std::string soundspeed_ns = ros::param::param<std::string>("~soundspeed_ns", "0");
  sub_["SoundSpeed"] = nodeHandle().subscribe<ds_sensor_msgs::SoundSpeed>(
      soundspeed_ns, 1, boost::bind(&DsDeadreck::onSoundSpeedMsg, this, _1));
  std::string modelstate_ns = ros::param::param<std::string>("~modelstate_ns", "0");
  sub_["ModelState"] = nodeHandle().subscribe<ds_nav_msgs::ModelState>(
      modelstate_ns, 1, boost::bind(&DsDeadreck::onModelStateMsg, this, _1));
}

void DsDeadreck::setupPublishers()
{
  DsNav::setupPublishers();

  pub_["PoseStamped"] = nodeHandle().advertise<geometry_msgs::PoseStamped>("pose_world", 1);

  std::string dvlNavState_ns = ros::param::param<std::string>("~navstate_ns", "0");
  pub_["NavState"] = nodeHandle().advertise<ds_nav_msgs::NavState>(dvlNavState_ns, 1);

  std::string deadreck_ns = ros::param::param<std::string>("~deadreck_topic", "/deadreck");
  pub_["DeadReck"] = nodeHandle().advertise<ds_nav_msgs::DeadReck>(deadreck_ns, 1);
}

void DsDeadreck::setupTimers()
{
  // Setup common base class parameters
  DsNav::setupTimers();

  tmr_["SAC"] = nodeHandle().createTimer(ros::Duration(1.0), boost::bind(&DsDeadreck::onSacTimer, this, _1));
}

void DsDeadreck::setupServices()
{
  DsNav::setupServices();

  auto nh = nodeHandle();
  reset_dvl_srv_ = nh.advertiseService<ds_nav_msgs::ResetDvl::Request, ds_nav_msgs::ResetDvl::Response>(
      ros::this_node::getName() + "/reset_dvl", boost::bind(&DsDeadreck::_reset_dvl_cmd, this, _1, _2));
}

void DsDeadreck::setup()
{
  // Get everything ready
  DsNav::setup();

  // FINALLY initialize TF
  setupTf();
}

void DsDeadreck::setupTf()
{
  // Lookup Tf transforms for gyro, depth, dvl. Sound speed does not have a frame associated with it, as it is not
  // needed
  // Loop here until we receive transformations for all the offsets that we need
  while (true)
  {
    try
    {
      ROS_INFO_STREAM("DVL   transform: \"" << dvl_frame_ << "\" -> \"" << target_frame_ << "\"");
      ROS_INFO_STREAM("Gyro  transform: \"" << gyro_ins_frame_ << "\" -> \"" << target_frame_ << "\"");
      ROS_INFO_STREAM("Depth transform: \"" << depth_frame_ << "\" -> \"" << target_frame_ << "\"");
      ins_.setTransform(
          tf2::transformToEigen(getTfBuffer().lookupTransform(target_frame_, gyro_ins_frame_, ros::Time(0))));
      gyro_.setTransform(
          tf2::transformToEigen(getTfBuffer().lookupTransform(target_frame_, gyro_ins_frame_, ros::Time(0))));
      dvl_.setTransform(tf2::transformToEigen(getTfBuffer().lookupTransform(target_frame_, dvl_frame_, ros::Time(0))));
      depthPressure_.setTransform(
          tf2::transformToEigen(getTfBuffer().lookupTransform(target_frame_, depth_frame_, ros::Time(0))));
      break;
    }
    catch (tf2::TransformException& ex)
    {
      ROS_WARN("%s", ex.what());
      ROS_WARN("Waiting for tf2 transforms...");
      ros::Duration(1.0).sleep();
    }
  }
}
}
