/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_DEADRECK_H
#define DS_DEADRECK_H

#include <Eigen/Core>
#include <Eigen/Geometry>
#include "tf2_eigen/tf2_eigen.h"
#include "ds_nav/ds_nav.h"
#include "ds_nav/ds_nav_sensor.h"
#include "ds_base/ds_process.h"
#include "ds_sensor_msgs/DepthPressure.h"
#include "ds_sensor_msgs/Dvl.h"
#include "ds_sensor_msgs/Gyro.h"
#include "ds_sensor_msgs/Ins.h"
#include "ds_sensor_msgs/SoundSpeed.h"
#include "ds_nav_msgs/ModelState.h"
#include "ds_nav_msgs/NavState.h"
#include "ds_nav_msgs/DeadReck.h"
#include "geometry_msgs/Quaternion.h"
#include "geometry_msgs/TransformStamped.h"
#include "ds_nav_msgs/ResetDvl.h"

namespace ds_deadreck
{
class DsDeadreck : public ds_nav::DsNav
{
public:
  DsDeadreck();

  DsDeadreck(int argc, char* argv[], const std::string& name);
  ~DsDeadreck() override;

  void initialize(void);

  /// @brief Receives dvl data and performs dvl integration through a time step.
  ///
  /// This assumes that DVL data is supplied in the DVL instrument frame, and the
  /// signs are consistent with the PD5 notation (dvl moving, bottom stationary)
  /// and not with the PD0 notation (dvl stationary, bottom moving).
  /// X velocity is positive on the beam 1->2 axis (X)
  /// Y velocity is positive on the beam 4->3 axis (Y)
  /// Z velocity is positive UP
  /// Alignment between vehicle body and DVL under this convention should be 135 degrees
  void onDvlMsg(const ds_sensor_msgs::Dvl::ConstPtr& msg);

  void onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg);

  void onInsMsg(const ds_sensor_msgs::Ins::ConstPtr& msg);

  void onDepthPressureMsg(const ds_sensor_msgs::DepthPressure::ConstPtr& msg);

  void onSoundSpeedMsg(const ds_sensor_msgs::SoundSpeed::ConstPtr& msg);

  void onModelStateMsg(const ds_nav_msgs::ModelState::ConstPtr& msg);

  void onSacTimer(const ros::TimerEvent& event);

  void resetDvlPosition(double lat, double lon);

  void resetDvlPositionXY(double easting, double northing);

  void updateOrigin(double lat, double lon);

  Eigen::Vector3d integrate(Eigen::Vector3d vel, double dt);

  void setDvlTimeout(double timeout);

  void updateRph(double roll, double pitch, double heading, geometry_msgs::Quaternion q);

  void publish(ros::Time in);

  ds_nav_msgs::NavState& getState(void);

  Eigen::Vector3d rotateVel(Eigen::Vector3d in);

protected:
  void setupParameters() override;
  void setupPublishers() override;
  void setupSubscriptions() override;
  void setupTimers() override;
  void setupServices() override;
  void setup() override;

  void setupTf();

private:
  // New ds_nav_sensors
  ds_nav::DsNavSensor<ds_sensor_msgs::Dvl> dvl_;
  ds_nav::DsNavSensor<ds_sensor_msgs::Gyro> gyro_;
  ds_nav::DsNavSensor<ds_sensor_msgs::Ins> ins_;
  ds_nav::DsNavSensor<ds_sensor_msgs::DepthPressure> depthPressure_;
  ds_nav::DsNavSensor<ds_sensor_msgs::SoundSpeed> soundSpeed_;

  // Store a copy of last sensor messages received
  ds_nav_msgs::ModelState modelState_;
  ds_nav_msgs::NavState dvlNavState_;      // internal state, DVL position
  ds_nav_msgs::NavState vehicleNavState_;  // State-tosend
  ds_nav_msgs::DeadReck dr_;               // Internal state of deadreck to publish for introspection

  // Dvl pipeline private members
  Eigen::Vector3d dvl_scaled_;
  Eigen::Vector3d dvl_scaled_tfd_;
  Eigen::Vector3d dvl_scaled_tfd_world_;
  Eigen::Affine3d gyro_a_;
  Eigen::Affine3d gyro_tfd_;
  Eigen::Vector3d offset_;

  // Publishers, timers, subscribers
  std::unordered_map<std::string, ros::Publisher> pub_;
  std::unordered_map<std::string, ros::Subscriber> sub_;
  std::unordered_map<std::string, ros::Timer> tmr_;

  // Dvl timeout after which model velocities kick in
  double dvl_timeout_;

  // Default sound speed used until a sound speed message is received
  double default_ss_;

  // We have had some issues with bad sound velocities being reported by the DVL itself.  This shouldn't
  // really be possible, but is.  Just to be sure, add a clamp.
  // Maximum allowed sound speed reported by the DVL
  double dvlss_max_;

  // Minimum allowed sound speed reported by the DVL
  double dvlss_min_;

  // Value assumed to be the DVL sound velocity when actual sound velocity is out of range
  double dvlss_assumed_;

  // Frame names of this navigation node for odometry and map
  std::string odometry_frame_;
  std::string map_frame_;

  // Input frames
  std::string dvl_frame_;
  std::string gyro_ins_frame_;
  std::string depth_frame_;

  // Output target frame for nav solution
  std::string target_frame_;

  ros::Time time_last_good_bt_;
  ros::Time last_model_time_;

  // Service call for dvl reset
  ros::ServiceServer reset_dvl_srv_;
  bool _reset_dvl_cmd(const ds_nav_msgs::ResetDvl::Request& req, const ds_nav_msgs::ResetDvl::Response& resp);
};
}

#endif
