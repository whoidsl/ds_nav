/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "ds_vfr_to_navsatfix/ds_vfr_to_navsatfix.h"

namespace ds_nav{

VfrToNavsatfix::VfrToNavsatfix()
    : ds_base::DsProcess()
{
}

VfrToNavsatfix::VfrToNavsatfix(int argc, char* argv[], const std::string& name)
    : ds_base::DsProcess(argc, argv, name)
{
}

VfrToNavsatfix::~VfrToNavsatfix()
{
}


void
VfrToNavsatfix::on_vfr_msg(const ds_core_msgs::RawData& msg)
{
  std::string vfr_string(msg.data.data(), msg.data.data()+msg.data.size());

  double lat;
  double lon;
  double unixtime;
  int veh_id;
  char date[1024];
  char time[1024];
  double depth;
  char soln[1024];
  
  const auto n_parsed = sscanf(vfr_string.c_str(), "VFR %s %s %*d %d %s %lf %lf %lf", date, time, &veh_id,
			       soln,
			       &lon,
			       &lat,
			       &depth);

  if (n_parsed == 7)
    {
      std::string mySoln(soln);

      if (!mySoln.compare(vfr_filter))
	{
	  sensor_msgs::NavSatFix navsat;
	  navsat.header.stamp = ros::Time::now();

	  navsat.latitude = lat;
	  navsat.longitude = lon;
	  navsat.altitude = depth;
	  navsat.position_covariance_type = navsat.COVARIANCE_TYPE_UNKNOWN;
	  navsat.status.status = navsat.status.STATUS_FIX;
	  navsat.status.service = navsat.status.SERVICE_GPS;
      
	  m_navsatfix_pub.publish(navsat);
	}
    }


}

void
VfrToNavsatfix::setupConnections()
{
  ds_base::DsProcess::setupConnections();
  m_nmea_conn = addConnection("vfr", boost::bind(&VfrToNavsatfix::on_vfr_msg, this, _1));

  vfr_filter = ros::param::param<std::string>("~vfr_solution_filter", "SOLN_DEADRECK");
  
  auto navsat_topic = ros::param::param<std::string>("~navsat_topic", "/gyro");
  m_navsatfix_pub = nodeHandle().advertise<sensor_msgs::NavSatFix>(navsat_topic, 1000);
}


} //namespace
