/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_navaggregator/ds_navaggregator.h"
#include <limits>

namespace ds_navaggregator
{
DsNavAggregator::DsNavAggregator(int argc, char* argv[], const std::string& name) : DsNav(argc, argv, name)
{
  nodeHandle();
  ROS_INFO_STREAM(ros::this_node::getName());

  auto myUuid = uuid();
  std::copy(std::begin(myUuid), std::end(myUuid), std::begin(state_.ds_header.source_uuid));

  // Set all state output flags to invalid
  state_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.p.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.q.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.du_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.dv_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.dw_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.dp_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.dq_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  state_.dr_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
}

DsNavAggregator::~DsNavAggregator() = default;

void DsNavAggregator::reconfigureCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  for (auto iter = params.begin(); iter != params.end(); iter++)
  {
    if (*iter == enum_heading)
    {
      ROS_WARN_STREAM("\t\t\tPARAM INT Changed from \"" << enum_heading->GetPrevious() << "\" to \""
                                                        << enum_heading->Get() << "\"");
    }
    if (*iter == enum_pitchroll)
    {
      ROS_WARN_STREAM("\t\t\tPARAM INT Changed from \"" << enum_pitchroll->GetPrevious() << "\" to \""
                                                        << enum_pitchroll->Get() << "\"");
    }
    if (*iter == enum_xy)
    {
      ROS_WARN_STREAM("\t\t\tPARAM INT Changed from \"" << enum_xy->GetPrevious() << "\" to \"" << enum_xy->Get()
                                                        << "\"");
    }
    if (*iter == enum_depth)
    {
      ROS_WARN_STREAM("\t\t\tPARAM INT Changed from \"" << enum_depth->GetPrevious() << "\" to \"" << enum_depth->Get()
                                                        << "\"");
    }
  }
}

void DsNavAggregator::sendState(ros::Time in, geometry_msgs::Quaternion orientation)
{
  state_.header.stamp = in;
  state_.ds_header.io_time = in;
  if (!std::isfinite(state_.northing.value))
    state_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.easting.value))
    state_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.down.value))
    state_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.roll.value))
    state_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.pitch.value))
    state_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.heading.value))
    state_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.surge_u.value))
    state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.sway_v.value))
    state_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.heave_w.value))
    state_.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.p.value))
    state_.p.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.q.value))
    state_.q.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.r.value))
    state_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.du_dt.value))
    state_.du_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.dv_dt.value))
    state_.dv_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.dw_dt.value))
    state_.dw_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.dp_dt.value))
    state_.dp_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.dq_dt.value))
    state_.dq_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;
  if (!std::isfinite(state_.dr_dt.value))
    state_.dr_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_INVALID;

  aggregated_publisher_.publish(state_);

  sendTf("base_link", odometry_frame_, state_.easting.value, state_.northing.value, -state_.down.value, orientation);

  fix_.header = state_.header;
  navsat_pub_.publish(fix_);

  // fill in / publish the navstate message
  navstate_.header = state_.header;
  navstate_.ds_header = state_.ds_header;
  navstate_.lon = fix_.longitude;
  navstate_.lat = fix_.latitude;
  navstate_.down = state_.down.valid ? state_.down.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.roll = state_.roll.valid ? state_.roll.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.pitch = state_.pitch.valid ? state_.pitch.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.heading = state_.heading.valid ? state_.heading.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.surge_u = state_.surge_u.valid ? state_.surge_u.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.sway_v = state_.sway_v.valid ? state_.sway_v.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.heave_w = state_.heave_w.valid ? state_.heave_w.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.p = state_.p.valid ? state_.p.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.q = state_.q.valid ? state_.q.value : std::numeric_limits<double>::quiet_NaN();
  navstate_.r = state_.r.valid ? state_.r.value : std::numeric_limits<double>::quiet_NaN();
  tf2::Quaternion q;
  if (state_.roll.valid && state_.pitch.valid && state_.heading.valid) {
    q.setRPY(state_.roll.value, state_.pitch.value, state_.heading.value);
    navstate_.orientation = tf2::toMsg(q);
  } else {
    navstate_.orientation.x = std::numeric_limits<double>::quiet_NaN();
    navstate_.orientation.y = std::numeric_limits<double>::quiet_NaN();
    navstate_.orientation.z = std::numeric_limits<double>::quiet_NaN();
    navstate_.orientation.w = std::numeric_limits<double>::quiet_NaN();
  }
  navstate_pub_.publish(navstate_);
}

void DsNavAggregator::setupSubscriptions(void)
{
  DsProcess::setupSubscriptions();

  std::string ins_ns = ros::param::param<std::string>("~ins_ns", "0");
  ROS_INFO_STREAM(ins_ns);
  sub_["Ins"] = nodeHandle().subscribe<ds_sensor_msgs::Ins>(
      ins_ns, 1, boost::bind(&DsNavAggregator::onInsMsg, this, _1, "Phins_ins"));
  std::string gyro_ns = ros::param::param<std::string>("~gyro_ns", "0");
  ROS_INFO_STREAM(gyro_ns);
  sub_["Gyro"] = nodeHandle().subscribe<ds_sensor_msgs::Gyro>(
      gyro_ns, 1, boost::bind(&DsNavAggregator::onGyroMsg, this, _1, "Phins_gyro"));
  std::string depth_ns = ros::param::param<std::string>("~depth_ns", "0");
  ROS_INFO_STREAM(depth_ns);
  sub_["Depth"] = nodeHandle().subscribe<ds_sensor_msgs::DepthPressure>(
      depth_ns, 1, boost::bind(&DsNavAggregator::onDepthPressureMsg, this, _1, "Paro"));
  std::string state_ns = ros::param::param<std::string>("~state_ns", "0");
  ROS_INFO_STREAM(state_ns);
  sub_["State"] = nodeHandle().subscribe<ds_nav_msgs::NavState>(
      state_ns, 1, boost::bind(&DsNavAggregator::onNavStateMsg, this, _1, "Deadreck"));
  std::string decktest_topic = ros::param::param<std::string>("~decktest_topic", "/decktest");
  ROS_INFO_STREAM(decktest_topic);
  sub_["Decktest"] = nodeHandle().subscribe<ds_nav_msgs::NavState>(
      decktest_topic, 1, boost::bind(&DsNavAggregator::onNavStateMsg, this, _1, "Decktest"));
  std::string phinsbin_topic = ros::param::param<std::string>("~phinsbin_topic", "/phinsbin");
  ROS_INFO_STREAM(phinsbin_topic);
  sub_["Phinsbin"] = nodeHandle().subscribe<ds_sensor_msgs::PhinsStdbin3>(
      phinsbin_topic, 1, boost::bind(&DsNavAggregator::onPhinsStdbin3Msg, this, _1, "Phinsbin"));

  auto nh = nodeHandle();
  conn_ = ds_param::ParamConnection::create(nh);

  enum_heading = conn_->connect<ds_param::EnumParam>(ros::this_node::getName() + "/enum_heading", true);
  enum_heading->addNamedValue("Phins_ins", 0);
  enum_heading->addNamedValue("Phins_gyro", 1);

  enum_pitchroll = conn_->connect<ds_param::EnumParam>(ros::this_node::getName() + "/enum_pitchroll", true);
  enum_pitchroll->addNamedValue("Phins_ins", 0);
  enum_pitchroll->addNamedValue("Phins_gyro", 1);

  enum_xy = conn_->connect<ds_param::EnumParam>(ros::this_node::getName() + "/enum_xy", true);
  enum_xy->addNamedValue("Deadreck", 0);
  enum_xy->addNamedValue("Decktest", 1);
  enum_xy->addNamedValue("Phinsbin", 2);

  enum_depth = conn_->connect<ds_param::EnumParam>(ros::this_node::getName() + "/enum_depth", true);
  enum_depth->addNamedValue("Paro", 0);

  enum_sync = conn_->connect<ds_param::EnumParam>(ros::this_node::getName() + "/enum_sync", true);
  enum_sync->addNamedValue("Phins_ins", 0);
  enum_sync->addNamedValue("Phins_gyro", 1);

  conn_->setCallback(boost::bind(&DsNavAggregator::reconfigureCallback, this, _1));
}

void DsNavAggregator::setupParameters(void)
{
  DsNav::setupParameters();
  // Output frames
  odometry_frame_ = ros::param::param<std::string>("~odometry_frame", "odom_dr");
  map_frame_ = ros::param::param<std::string>("~map_frame", "map_dr");
}

void DsNavAggregator::setupPublishers(void)
{
  DsNav::setupPublishers();

  pose_publisher_ = nodeHandle().advertise<geometry_msgs::PoseStamped>(ros::this_node::getName() + "/odom/pose", 1);
  aggregated_publisher_ = nodeHandle().advertise<ds_nav_msgs::AggregatedState>(ros::this_node::getName() + "/state", 1);

  latitude_topic_ = ros::param::param<std::string>("~navsat_topic", "0");
  navsat_pub_ = nodeHandle().advertise<sensor_msgs::NavSatFix>(latitude_topic_, 1);

  navstate_pub_ = nodeHandle().advertise<ds_nav_msgs::NavState>(ros::this_node::getName() + "/navstate", 1);
}

void DsNavAggregator::onInsMsg(const ds_sensor_msgs::Ins::ConstPtr& msg, std::string configType)
{
  ins_.push_back(*msg);

  if (enum_heading->getValueByName() == configType)
  {
    // Compute heading rate
    if (ins_.full())
    {
      double angular_separation =
          ds_util::angular_separation_radians(ins_.getPrevious().heading, ins_.getLast().heading);
      state_.r.value = angular_separation / (ins_.getLast().header.stamp - ins_.getPrevious().header.stamp).toSec();
      state_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    }

    state_.heading.value = msg->heading;
    state_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.pitch.value = -msg->pitch;
    state_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.roll.value = msg->roll;
    state_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  }
  if (enum_sync->getValueByName() == configType)
  {
    sendState(msg->header.stamp, msg->orientation);
  }
}

void DsNavAggregator::onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg, std::string configType)
{
  gyro_.push_back(*msg);

  if (enum_heading->getValueByName() == configType)
  {
    // Compute heading rate
    if (gyro_.full())
    {
      double angular_separation =
          ds_util::angular_separation_radians(gyro_.getPrevious().heading, gyro_.getLast().heading);
      state_.r.value = angular_separation / (gyro_.getLast().header.stamp - gyro_.getPrevious().header.stamp).toSec();
      state_.r.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    }

    state_.heading.value = msg->heading;
    state_.heading.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.pitch.value = -msg->pitch;
    state_.pitch.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.roll.value = msg->roll;
    state_.roll.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  }

  if (enum_sync->getValueByName() == configType)
  {
    sendState(msg->header.stamp, msg->orientation);
  }
}

void DsNavAggregator::onDepthPressureMsg(const ds_sensor_msgs::DepthPressure::ConstPtr& msg, std::string configType)
{
  if (msg->median_depth_filter_ok)
  {
    depth_.push_back(*msg);

    if (enum_depth->getValueByName() == configType)
    {
      state_.down.value = msg->depth;
      state_.down.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
      fix_.altitude = -msg->depth;
      // Heave comes from depth rate
      if (depth_.full())
      {
        state_.heave_w.value = (depth_.getLast().depth - depth_.getPrevious().depth) /
                               (depth_.getLast().header.stamp - depth_.getPrevious().header.stamp).toSec();
        state_.heave_w.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
      }
    }
  }
  else
  {
    ROS_ERROR_STREAM("Median depth invalid, skipping aggregation of depth data");
  }
}

void DsNavAggregator::onNavStateMsg(const ds_nav_msgs::NavState::ConstPtr& msg, std::string configType)
{
  navState_[configType].push_back(*msg);

  if (enum_xy->getValueByName() == configType)
  {
    state_.northing.value = msg->northing;
    state_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.easting.value = msg->easting;
    state_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.surge_u.value = msg->surge_u;
    state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.sway_v.value = msg->sway_v;
    state_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    fix_.latitude = msg->lat;
    fix_.longitude = msg->lon;
    // Compute surge acceleration for the controller
    if (navState_[configType].full())
    {
      state_.du_dt.value =
          (navState_[configType].getLast().surge_u - navState_[configType].getPrevious().surge_u) /
          (navState_[configType].getLast().header.stamp - navState_[configType].getPrevious().header.stamp).toSec();
      state_.du_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    }
  }
}

void DsNavAggregator::onPhinsStdbin3Msg(const ds_sensor_msgs::PhinsStdbin3::ConstPtr& msg, std::string configType)
{
  phinsbin_.push_back(*msg);

  if (enum_xy->getValueByName() == configType)
  {
    state_.northing.value = 0;// TODO - need to find local xy msg->northing;
    state_.northing.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.easting.value = 0;// TODO - need to find local xy msg->easting;
    state_.easting.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.surge_u.value = msg->heaveSurgeSway_speed_XVnH[0];//msg->surge_u; todo check order - 0 should be surge
    state_.surge_u.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    state_.sway_v.value = msg->heaveSurgeSway_speed_XVnH[1];//msg->sway_v; todo check order - 1 should be sway but may have to be negated for NED
    state_.sway_v.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
    fix_.latitude = msg->latitude;
    fix_.longitude = msg->longitude;
    // Compute surge acceleration for the controller
    state_.du_dt.value = msg->body_accel_XVn[0];
    state_.du_dt.valid = ds_nav_msgs::FlaggedDouble::VALUE_VALID;
  }
}

}
