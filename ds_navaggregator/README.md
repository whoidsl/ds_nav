# FRAME CONVENTIONS

The nav aggregator ALWAYS output data in the following frame:

* easting (position, world frame)
* northing (position, world frame)
* down (position, world frame)
* surge_u (forward velocity, vehicle frame)
* sway_v (starboard velocity, vehicle frame)
* heave_w (down velocity, vehicle frame)

# GENERAL INFO

This package contains a node (ds_navaggregator_node) that uses dynamic_reconfigure. Ideally, this node should be a dynamic_reconfigure server and at the same time a dynamic_reconfigure client, so that the node can be externally configured, and so that i can also autonomously reconfigure itself based on an internal logic.

Useful tools to do dynamic_reconfigure:

* rosrun rqt_reconfigure rqt_reconfigure: runs a GUI that can set parameters on dynamic_reconfigure

Default dynamic parameters can be loaded from .yaml files using this launch file line:

* <node name="dynamic_reconfigure_load" pkg="dynamic_reconfigure" type="dynparam" args="load /ds_navaggregator_node_name $(find my_package)/params/navaggregator.yaml" />

Params can be dumped to a .yaml file with:

* rosrun dynamic_reconfigure dynparam dump /ds_navaggregator_node_name navaggregator.yaml