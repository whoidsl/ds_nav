/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_NAVAGGREGATOR_H
#define DS_NAVAGGREGATOR_H

#include "ros/ros.h"
#include "ds_nav/ds_nav.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include <tf2_ros/transform_broadcaster.h>
#include "tf2_eigen/tf2_eigen.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_nav_msgs/NavState.h"
#include "ds_nav_msgs/FlaggedDouble.h"
#include "ds_sensor_msgs/Ins.h"
#include "ds_sensor_msgs/Gyro.h"
#include "ds_sensor_msgs/DepthPressure.h"
#include <ds_param/ds_param_conn.h>
#include <boost/optional/optional_io.hpp>
#include "ds_nav/ds_nav_sensor.h"
#include "sensor_msgs/NavSatFix.h"
#include "ds_util/angle.h"
#include "ds_sensor_msgs/PhinsStdbin3.h"

namespace ds_navaggregator
{
class DsNavAggregator final : public ds_nav::DsNav
{
public:
  DsNavAggregator(int argc, char* argv[], const std::string& name);
  ~DsNavAggregator() override;

  void reconfigureCallback(const ds_param::ParamConnection::ParamCollection& params);

  void sendState(ros::Time in, geometry_msgs::Quaternion orientation);

  void onNavStateMsg(const ds_nav_msgs::NavState::ConstPtr& msg, std::string configType);

  void onInsMsg(const ds_sensor_msgs::Ins::ConstPtr& msg, std::string configType);

  void onGyroMsg(const ds_sensor_msgs::Gyro::ConstPtr& msg, std::string configType);

  void onDepthPressureMsg(const ds_sensor_msgs::DepthPressure::ConstPtr& msg, std::string configType);

  void onPhinsStdbin3Msg(const ds_sensor_msgs::PhinsStdbin3::ConstPtr& msg, std::string configType);

protected:
  void setupSubscriptions() override;
  void setupParameters() override;
  void setupPublishers() override;

private:
  // State output by the aggregator for the controller
  ds_nav_msgs::AggregatedState state_;

  ds_nav::DsNavSensor<ds_sensor_msgs::Gyro> gyro_;
  ds_nav::DsNavSensor<ds_sensor_msgs::Ins> ins_;
  ds_nav::DsNavSensor<ds_sensor_msgs::DepthPressure> depth_;
  ds_nav::DsNavSensor<ds_sensor_msgs::PhinsStdbin3> phinsbin_;
  std::unordered_map<std::string, ds_nav::DsNavSensor<ds_nav_msgs::NavState> > navState_;

  ros::Publisher pose_publisher_;
  ros::Publisher aggregated_publisher_;

  // Publishers, timers, subscribers
  std::unordered_map<std::string, ros::Publisher> pub_;
  std::unordered_map<std::string, ros::Subscriber> sub_;
  std::unordered_map<std::string, ros::Timer> tmr_;

  ds_param::ParamConnection::Ptr conn_;

  ds_param::EnumParam::Ptr enum_heading;
  ds_param::EnumParam::Ptr enum_pitchroll;
  ds_param::EnumParam::Ptr enum_xy;
  ds_param::EnumParam::Ptr enum_depth;
  ds_param::EnumParam::Ptr enum_sync;

  // Output frames
  std::string odometry_frame_;
  std::string map_frame_;

  // Output latitude topic
  std::string latitude_topic_;
  ros::Publisher navsat_pub_;

  sensor_msgs::NavSatFix fix_;

  // Output navstate
  ros::Publisher navstate_pub_;

  ds_nav_msgs::NavState navstate_;
};
}

#endif
